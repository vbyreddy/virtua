var winston = require('winston');

module.exports.logger =  winston.createLogger({
    level: 'error',
    // format: winston.format.json(),
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
    ),
    transports: [
        new winston.transports.File({
            filename: 'Logs/errors.log'
        })
    ]
});

// export default logger;
