import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSidenavModule,
  MatTabsModule,
  MatCardModule,
  MatPaginatorModule,
  MatTableModule,
  MatDividerModule,
  MatSortModule,
  MatInputModule,
  MatFormFieldModule } from '@angular/material';
// import { FooterComponent } from '../../common/footer/footer.component';
// import { HeaderComponent } from '../../common/header/header.component';
// import { SharedModule } from '../../common/shared.module';
import { CommonModule } from '@angular/common';
import { FilterCustomerComponent,
  CustomerViewComponent
} from './customer.component';

// import { CommonServerService } from '../../Services/commonServer';
// import { CommonService } from '../../Services/commonService';



const routes: Routes = [
  {path: '', component: FilterCustomerComponent},
  {path: 'customercreate', component: CustomerViewComponent}
];

@NgModule({
  declarations: [FilterCustomerComponent,
    CustomerViewComponent
  ],
  imports: [FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatTabsModule,
    MatCardModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule, FilterCustomerComponent,
    // CustomerViewComponent
  ],
  providers: []
})

export class CustomerModule { }
