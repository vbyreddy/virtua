import { Component, OnInit, ViewChild, ElementRef, Renderer, ViewChildren, Directive, OnDestroy, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TabCommon, MyInput } from 'src/app/modules/Services/tab-common';
// import { UploadFiles } from '../../Services/uploadFiles';
import { HttpClient } from '@angular/common/http';
// import { DocumentModalComponent } from '../../ModalComponents/documentModal/documentModal';
// import { UserModalComponent } from '../../ModalComponents/userModal/userModal';
// import { RemarksModalComponent } from '../../ModalComponents/remarksModal/remarksModal';
// import { ModalDataService } from '../../Services/modalServiceData';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
// import { ServicesService } from '../../Services/services.service';
// import { CbsModalComponent } from '../../ModalComponents/CbsModal/CbsModal';
// import { ISubscription } from 'rxjs/Subscription';
// import { CommonServerService } from '../../Services/commonServer';
// import { ReviewApproveAuthService } from '../../Services/reviewApproveAuth.service';
// import { CommonService } from '../../Services/commonService';
// import { GlobalMraService } from '../../Services/globalMraService';
// import { EmailCheckModalComponent } from '../../ModalComponents/emailCheckModal/emailCheckModal';
// import { BranchValidModalComponent } from '../../ModalComponents/branchValidModal/branchValidModal';
// import { BasicNewModalComponent } from '../../ModalComponents/basicNewModal/basicNewModal';
// import { TokenService } from '../../providers/token.service';
import { saveAs } from 'file-saver';
// import { AdviceService } from './../../Services/adviceService';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

@Component({
  selector: 'app-filtercustomer',
  templateUrl: './customer.component.html',
  styleUrls: ['./Customer/customer.component.css']
})
export class FilterCustomerComponent implements OnInit {

  customer: any  = [];
  search: any  = { customerId: '' };
  defaultPage: any  = true;
  selectedItem: any  = {};
  recordsPerPage: any  = 10;
  noOfPages: any  = 0;
  currentPage: any  = 0;
  trxnMode;
  transactionCmd;
  pageData: any  = {};
  searchBy: any  = {};
  numberOfPagesNew: Number;
  customerDNew: any  = [];
  customerD: any  = [];
  pageIndex: any = 0;
  pageSize: any = 5;
  lowValue: any = 0;
  highValue: any = 50;
  displayedColumns: string[] = ['customerId', 'customerName', 'borrowerAccountNumber', 'trxnStatus', 'lastUpdated', 'userId'];
  dataSource: any = [];
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event) {
    // let jj = this.CommonService.getUser();
    // //
    // this.http.post("/user/logout", this.CommonService.getEncryptedData(jj)).toPromise()
    //   .then(
    //     res => { // Success
    //       // //
    //       this._tokenService.delete();
    //     }
    //   );
  }

  // tslint:disable-next-line:no-shadowed-variable max-line-length
  constructor(
    // private CommonService: CommonService,
    private http: HttpClient,
    // private _tokenService: TokenService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    // private servicesService: ServicesService,
    // private CommonServerService: CommonServerService,
    // private ReviewApproveAuthService: ReviewApproveAuthService
    ) { }


  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params) {
        this.searchBy.customerId = params.customerId;
      }
    });
    // this.CommonServerService.CustomerMGetAll().subscribe(payload => {
    //   payload = JSON.parse(this.CommonService.getDecryptedData(payload));
    //   this.customerD = this.CommonService.dateFilter(payload[`data`]);
    //   console.log("this.customerD----", this.customerD);
    //   this.numberOfPagesByCustomer();
    // });

    this.http.get('https://192.168.1.13:4400/mastersCustomers/entries').toPromise().then(payload => {
      console.log('payload', payload);
      // payload = JSON.parse(this.CommonService.getDecryptedData(payload));
      this.customerD = payload[`data`];
      this.dataSource = new MatTableDataSource(this.customerD);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }).catch(error => {
      console.log('error', error);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  open(item) {
    // this.selectedItem = item;
    // this.ReviewApproveAuthService.reviewApproveAuth(this.selectedItem, this.selectedItem.trxnStatus);
  }

  customerView(typeValue, status) {
    // this.defaultPage = false;
    // if (typeValue === 'create') {
    //   this.pageData = { type: typeValue, disableAll: false, selectedItem: '', Status: status };
    // } else if (typeValue === 'update') {
    //   this.pageData = { type: typeValue, disableAll: false, selectedItem: this.selectedItem, Status: status };
    // } else if (typeValue === 'review' || typeValue === 'approve' || typeValue === 'enquiry') {
    //   this.pageData = { type: typeValue, disableAll: true, selectedItem: this.selectedItem, Status: status };
    // }
    // this.servicesService.sendActionToPerform(this.pageData);
  }
}


@Component({
  selector: 'app-customer-view',
  templateUrl: './Customer/customer.component.html',
  styleUrls: ['./Customer/customer.component.css']
})
export class CustomerViewComponent {
//   subscription: ISubscription;
  customer: any = {};
//   i: any;
//   j: any;
//   errorMsg = {};
//   keey1: any;
//   tab: any;
//   transactionMode: any;
//   hideTabs = true;
//   disableAll: any;
//   modalRef: BsModalRef;
//   addedToMainDatabase: any;
//   mandatoryFields = false;
//   totalRows = 1;
//   uploadFile: any;
//   fileUploadError = false;
//   fileUploadSize = false;
//   fileUploadContent = false;
//   fileUploadCrash = false;
//   mandatoryRefNo = false;
//   ReadOnlyField: any;
//   CIFData: any;
//   allValidationErrors;
//   allValidationErrorsWithTabValue;
//   errorPresent: any = false;
//   trxnStatus: string;
//   customerAlreadyExisted = false;
//   sendDataOption: any = {};
//   transactionCmd: string;
//   trxnActualMode: any;
//   mandatoryFieldsReviewApprove = false;
//   mandatoryClarifyRemarks = false;
//   forUpdateClarify: any;
//   files: any;
//   currentUserLogIn: any = {};
//   // activeData;
//   activeData;
//   InactiveData;
//   repeatedData: any = {};
//   disabledfiled;
//   serialNumber: any = [];
//   errorMsg1 = {};
//   flowData: any = {};
//   options = {
//     moduleName: 'Masters',
//     transactionName: 'Customer'
//   };
//   fileDuplicate = false;
//   viewFile = [];
//   dbError = false;
//   ReadOnlyemail: any;
//   ReadOnlycustomerName: any;
//   ReadOnlycustomerAddress1: any;
//   ReadOnlymobileNo: any;
//   ReadOnlyborrowerAccountNumber: any;
//   emailError;
//   smsError;
//   constructor(private modalService: BsModalService, private router: Router, private _tokenService: TokenService, private _modalService: ModalDataService, private UploadFiles: UploadFiles, private http: HttpClient, private servicesService: ServicesService, private CommonServerService: CommonServerService, private CommonService: CommonService, private GlobalMraService: GlobalMraService, private AdviceService : AdviceService) { }
//   @ViewChildren(MyInput) inputs;
//   @HostListener('window:beforeunload', ['$event'])
//   public beforeunloadHandler($event) {
//     let jj = this.CommonService.getUser();
//     //
//     this.http.post("/user/logout", this.CommonService.getEncryptedData(jj)).toPromise()
//       .then(
//         res => { // Success
//           // //
//           this._tokenService.delete();
//         }
//       );
//   }
//   ngOnInit() {
//     this.currentUserLogIn = this.CommonService.getUser();
//     this.customer.creationDate = new Date();
//     this.customer.trxnName = 'Customer';
//     this.subscription = this.servicesService.Message.subscribe(data => {
//       this.transactionMode = data.type;
//       this.forUpdateClarify = data.selectedItem.trxnStatus;
//       if (data.type === 'create') {
//         this.customer.uploadedFiles = [];
//         this.customer.remarksHistory = [];
//         this.customer.addedcustomerData = [];
//         this.customer.existingClient = 'Yes';
//         this.InactiveData = 'Inactive';
//         // this.customer.activeData = 'inactive';
//       } else {
//         if (data.type === 'enquiry' || data.type === 'review' || data.type === 'approve') {
//           this.customer = data.selectedItem;
//           this.customer.uploadedFiles = JSON.parse(data.selectedItem.uploadedDocs);
//           if (data.selectedItem.remarksHistory) {
//             this.customer.remarksHistory = JSON.parse(data.selectedItem.remarksHistory);
//           } else {
//             this.customer.remarksHistory = [];
//           }
//           if (data.selectedItem.signatoryData) {
//             this.customer.addedcustomerData = JSON.parse(data.selectedItem.signatoryData);
//           } else {
//             this.customer.addedcustomerData = [];
//           }
//         } else if (data.type === 'update') {
//           this.activeData = 'active';
//           this.customer = data.selectedItem;
//           this.customer.uploadedFiles = JSON.parse(data.selectedItem.uploadedDocs);
//           if (data.selectedItem.remarksHistory) {
//             this.customer.remarksHistory = JSON.parse(data.selectedItem.remarksHistory);
//           } else {
//             this.customer.remarksHistory = [];
//           }
//           if (data.selectedItem.signatoryData) {
//             this.customer.addedcustomerData = JSON.parse(data.selectedItem.signatoryData);
//           } else {
//             this.customer.addedcustomerData = [];
//           }
//         }
//       }
//       this.disableAll = data.disableAll;
//       this.trxnActualMode = data.type;
//       this.transactionCmd = this.transactionMode;
//       this.tab = 1;
//       this.hideTabs = true;
//     });
//   }
//   ngOnDestroy() {
//     this.subscription.unsubscribe();
//   }
//   addRow = () => {
//     // this.totalRows++;
//     // this.totalDeferredRows++;
//     this.allValidationErrors = [];
//     this.allValidationErrorsWithTabValue = [];
//     this.CommonServerService.customerValidationAscustomer(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//       if (response['status'] === 'validationError') {
//         this.errorPresent = true;
//         this.showTabPage(0);
//         this.i = 0;
//         this.hideTabs = false;
//         this.validationErrorMethod1(response['data']);

//       } else if (response['status'] === 'success') {
//         this.errorPresent = false;
//         // if ((this.dealSLength.length == 0 || this.dealSLength.length == '0') ) {
//         this.errorMsg1 = {};
//         this.customer.addedcustomerData.push({ authorizedSignatoryId: this.customer.authorizedSignatoryId, authorizedSignatoryName: this.customer.authorizedSignatoryName, authorizedSignatoryEmail: this.customer.authorizedSignatoryEmail });
//         this.customer.authorizedSignatoryId = '';
//         this.customer.authorizedSignatoryName = '';
//         this.customer.authorizedSignatoryEmail = '';
//         console.log("this.customer.addedcustomerData", this.customer.addedcustomerData);
//         // }
//       }
//     });
//   }

//   removeRow = (i) => {
//     this.customer.addedcustomerData.splice(i, 1);
//   }

//   showTabPage(tabIndex) {
//     this.tab = tabIndex;
//     this.j = 1;
//   }

//   checkAvailabilityForCIF(customerId) {

//     if (this.customer.customerId) {
//       if (!String(this.customer.customerId).match(/^[a-zA-Z0-9\s+?/:.,()\'-]*$/)) {
//         this.errorMsg['customerId'] = 'Customer Id is Alpha Numeric';
//       } else {
//         this.errorMsg['customerId'] = '';
//         this.CommonServerService.CustomerMGetById(this.customer.customerId).subscribe(response => {
//           response = JSON.parse(this.CommonService.getDecryptedData(response));
//           if (response['status'] === 'success') {
//             this.errorMsg['customerId'] = ''; // for blank startup validation
//             this.customerAlreadyExisted = true;
//           } else {
//             this.customerAlreadyExisted = false;
//           }
//         });
//       }
//     }
//   }

//   checkAvailabilityDuplicate(NameData, MatchData) {

//     if (MatchData === 'Telephone') {
//       if (this.customer.telephone) {
//         this.CommonServerService.CustomerMGetDuplicateTelephone(this.customer.telephone).subscribe(response => {
//           response = JSON.parse(this.CommonService.getDecryptedData(response));
//           if (response['status'] === 'success') {
//             // this.customerAlreadyExisted = true;
//             this.repeatedData = {
//               AllData: 'telephoneCust',
//               dataduplicate: this.customer.telephone
//             }
//             this._modalService.setData(this.repeatedData);
//             this.modalRef = this.modalService.show(EmailCheckModalComponent);
//             this.modalRef.content.onClose.subscribe(result => {
//               if (result === 'No') {
//                 this.customer.telephone = '';
//               }
//             });
//           } else {
//             // this.customerAlreadyExisted = false;
//           }
//         });
//       }
//     } else if (MatchData === 'MobileCust') {
//       if (this.customer.mobileNo) {
//         this.CommonServerService.CustomerMGetDuplicateMobile(this.customer.mobileNo).subscribe(response => {
//           response = JSON.parse(this.CommonService.getDecryptedData(response));
//           if (response['status'] === 'success') {
//             // this.customerAlreadyExisted = true;
//             this.repeatedData = {
//               AllData: 'mobileCust',
//               dataduplicate: this.customer.mobileNo
//             }
//             this._modalService.setData(this.repeatedData);
//             this.modalRef = this.modalService.show(EmailCheckModalComponent);
//             this.modalRef.content.onClose.subscribe(result => {
//               if (result === 'No') {
//                 this.customer.mobileNo = '';
//               }
//             });
//           } else {
//             // this.customerAlreadyExisted = false;
//           }
//         });
//       }
//     } else if (MatchData === 'EmailCust') {
//       if (this.customer.email) {
//         this.CommonServerService.CustomerMGetDuplicateEmail(this.customer.email).subscribe(response => {
//           response = JSON.parse(this.CommonService.getDecryptedData(response));
//           if (response['status'] === 'success') {
//             // this.customerAlreadyExisted = true;
//             this.repeatedData = {
//               AllData: 'emailCust',
//               dataduplicate: this.customer.email
//             }
//             this._modalService.setData(this.repeatedData);
//             this.modalRef = this.modalService.show(EmailCheckModalComponent);
//             this.modalRef.content.onClose.subscribe(result => {
//               if (result === 'No') {
//                 this.customer.email = '';
//               }
//             });
//           } else {
//             // this.customerAlreadyExisted = false;
//           }
//         });
//       }
//     }



//   }
//   Selected(checkTab) {
//     if (this.keey1 !== undefined && this.keey1 !== undefined && this.keey1 !== '' && this.j < 20) {
//       this.inputs.toArray().some(myInput => myInput.focusIf(this.keey1));
//       this.j = this.j + 1;
//     } else {
//       this.j = 1;
//       this.keey1 = '';
//     }
//     return this.tab === checkTab;
//   }
//   openServicefromBank() {
//     // this.CommonServerService.CustomerMFetchFrombank(this.customer.customerIdNew).subscribe(response => {
//     //   console.log("payload", response);
//     //   if (response['RESPONSE']['_text'] === 'Success') {
//     //     this.ReadOnlyField = 'true';
//     //     var ACCOUNTS = response['ACCOUNTS']['_text'];
//     //     ACCOUNTS = ACCOUNTS.split("|");
//     //     var CUST_NAME = response['CUST_NAME']['_text'];
//     //     CUST_NAME = CUST_NAME.split("![CDATA[");
//     //         console.log("CUST_NAME", CUST_NAME);
//     //     CUST_NAME[1] = CUST_NAME[1].split("]]");
//     //     var CUST_ADDRESS = response['CUST_ADDRESS']['_text'];
//     //     CUST_ADDRESS = CUST_ADDRESS.split("![CDATA[");
//     //     CUST_ADDRESS[1] = CUST_ADDRESS[1].split("]]");
//     //     var MOBILE_NO = response['MOBILE_NO']['_text'];
//     //     MOBILE_NO = MOBILE_NO.split("![CDATA[");
//     //     MOBILE_NO[1] = MOBILE_NO[1].split("]]");
//     //     var EMAIL_ID = response['EMAIL_ID']['_text'];
//     //     EMAIL_ID = EMAIL_ID.split("![CDATA[");
//     //     EMAIL_ID[1] = EMAIL_ID[1].split("]]");
//     //     this.errorMsg['customerIdNew'] = ''
//     //     this.customer.customerName = CUST_NAME[1][0];
//     //     this.customer.customerAddress1 = CUST_ADDRESS[1][0];
//     //     this.customer.email = EMAIL_ID[1][0];
//     //     this.customer.mobileNo = MOBILE_NO[1][0];
//     //     this.customer.borrowerAccountNumber = ACCOUNTS[2];
//     //     this.customer.solId = ACCOUNTS[1]
//     //       } else {
//     //         this.ReadOnlyField = 'false';
//     //     this.errorMsg['customerIdNew'] = 'Customer Id not found';
//     //     this.customer.customerName = '';
//     //     this.customer.customerAddress1 = '';
//     //     this.customer.email = '';
//     //     this.customer.mobileNo = '';
//     //     this.customer.borrowerAccountNumber = '';
//     //   }
//     //   // this.customer = response['custAccNo2'];
//     // });
//     if (this.customer.customerIdNew == null || this.customer.customerIdNew == undefined || this.customer.customerIdNew == '') {
//       this.errorMsg['customerIdNew'] = 'Enter customer Id';
//     } else {
//       this.errorMsg['customerIdNew'] = '';
//       this.CommonServerService.CustomerMFetchFrombank(this.customer.customerIdNew).subscribe(response => {
//         if (response['SMSAWAY']['DETAILS']) {
//           if (response['SMSAWAY']['DETAILS']['RESPONSE']['_text'] == 'Success') {
//             this.ReadOnlyField = 'true';
//             var ACCOUNTS = response['SMSAWAY']['DETAILS']['ACCOUNTS']['_text'];
//             ACCOUNTS = ACCOUNTS.split("|");
//             var CUST_NAME = response['SMSAWAY']['DETAILS']['CUST_NAME']['_text'];
//             CUST_NAME = CUST_NAME.split("![CDATA[");
//             CUST_NAME[1] = CUST_NAME[1].split("]]");
//             var CUST_ADDRESS = response['SMSAWAY']['DETAILS']['CUST_ADDRESS']['_text'];
//             CUST_ADDRESS = CUST_ADDRESS.split("![CDATA[");
//             CUST_ADDRESS[1] = CUST_ADDRESS[1].split("]]");
//             var custData = CUST_ADDRESS[1][0].split("|");
//             var MOBILE_NO = response['SMSAWAY']['DETAILS']['MOBILE_NO']['_text'];
//             MOBILE_NO = MOBILE_NO.split("![CDATA[");
//             MOBILE_NO[1] = MOBILE_NO[1].split("]]");
//             var EMAIL_ID = response['SMSAWAY']['DETAILS']['EMAIL_ID']['_text'];
//             EMAIL_ID = EMAIL_ID.split("![CDATA[");
//             EMAIL_ID[1] = EMAIL_ID[1].split("]]");
//             this.errorMsg['customerIdNew'] = ''
//             this.customer.customerName = CUST_NAME[1][0];
//             this.customer.customerAddress1 = custData[0];
//             this.customer.customerAddress2 = custData[1];
//             this.customer.customerAddress3 = custData[2];
//             this.customer.email = EMAIL_ID[1][0];
//             this.customer.mobileNo = MOBILE_NO[1][0];
//             this.customer.borrowerAccountNumber = ACCOUNTS[2];
//             this.customer.solId = ACCOUNTS[1];
//             if (this.customer.email) {
//               this.ReadOnlyemail = 'true';
//             } else {
//               this.ReadOnlyemail = 'false';
//             }

//             if (this.customer.customerName) {
//               this.ReadOnlycustomerName = 'true';
//             } else {
//               this.ReadOnlycustomerName = 'false';
//             }

//             if (this.customer.customerAddress1) {
//               this.ReadOnlycustomerAddress1 = 'true';
//             } else {
//               this.ReadOnlycustomerAddress1 = 'false';
//             }

//             if (this.customer.mobileNo) {
//               this.ReadOnlymobileNo = 'true';
//             } else {
//               this.ReadOnlymobileNo = 'false';
//             }

//             if (this.customer.borrowerAccountNumber) {
//               this.ReadOnlyborrowerAccountNumber = 'true';
//             } else {
//               this.ReadOnlyborrowerAccountNumber = 'false';
//             }
//           }
//         } else {
//           //if (response['SMSAWAY']['MESSAGE']['RESPONSE']['_text'] == 'FAILURE') {
//           this.ReadOnlyemail = 'false';
//           this.ReadOnlyborrowerAccountNumber = 'false';
//           this.ReadOnlymobileNo = 'false';
//           this.ReadOnlycustomerAddress1 = 'false';
//           this.ReadOnlycustomerName = 'false';
//           this.errorMsg['customerIdNew'] = 'Customer Id not found';
//           this.customer.customerName = '';
//           this.customer.customerAddress1 = '';
//           this.customer.customerAddress2 = '';
//           this.customer.customerAddress3 = '';
//           this.customer.email = '';
//           this.customer.mobileNo = '';
//           this.customer.borrowerAccountNumber = '';
//           this.customer.branchName = '';
//           this.customer.solId = '';
//         }
//         //}
//         // this.customer = response['custAccNo2'];
//       });
//     }

//   }

//   // checkAvailabilityDupicateMobile(mobileNoD) {
//   //   console.log("enter or not here");

//   //   if (this.customer.mobileNo) {
//   //     // if (!String(this.customer.customerId).match(/^[a-zA-Z0-9\s+?/:.,()\'-]*$/)) {
//   //     //   this.errorMsg['customerId'] = 'customerId is Alpha Numeric';
//   //     // } else {
//   //       // this.errorMsg['customerId'] = '';
//   //       this.CommonServerService.customerGetMobileN(this.customer.mobileNo).subscribe(response => {
//   //         if (response['status'] === 'success') {
//   //           // this.errorMsg['customerId'] = ''; // for blank startup validation
//   //           this.customerAlreadyExisted = true;
//   //           this._modalService.setData('mobile');
//   //           this.modalRef = this.modalService.show(EmailCheckModalComponent);
//   //         } else {
//   //           this.customerAlreadyExisted = false;
//   //         }
//   //       });
//   //     // }
//   //   }
//   // }

//   changeTypeOfCsutomer() {
//     if (this.customer.typeOfCustomer === 'Company' || this.customer.typeOfCustomer === 'Individual' || this.customer.typeOfCustomer === 'HUF' || this.customer.typeOfCustomer === 'Partnership' || this.customer.typeOfCustomer === 'Proprietorship' || this.customer.typeOfCustomer === 'Trust' || this.customer.typeOfCustomer === 'AOP') {
//       this.customer.customerTypeOther = '';
//     }
//   }
//   changeTypeOfRole() {
//     if (this.customer.customerRole === 'Borrower' || this.customer.customerRole === 'Coborrower' || this.customer.customerRole === 'Obligor' || this.customer.customerRole === 'Sponsor' || this.customer.customerRole === 'Guarantor' || this.customer.customerRole === 'Issuer') {
//       this.customer.roleOther = '';
//     }
//   }
//   opencustomerDocument = () => {
//     this.modalRef = this.modalService.show(DocumentModalComponent);
//     this.modalRef.content.data = { modalData: this.customer, correspondingFunction: 'openDealInitcustomer' };
//     this.modalRef.content.onClose.subscribe(result => {
//       this.customer = result;
//     });
//   }
//   openCbs = () => {
//     this.modalRef = this.modalService.show(CbsModalComponent);
//     this.modalRef.content.data = { modalData: this.customer, correspondingFunction: 'opencbsManage' };
//     this.modalRef.content.onClose.subscribe(result => {
//       this.customer = result;
//     });
//   }
//   openUserManage = () => {
//     this.modalRef = this.modalService.show(UserModalComponent);
//     this.modalRef.content.data = { modalData: this.customer, correspondingFunction: 'openUserManage' };
//     this.modalRef.content.onClose.subscribe(result => {
//       this.customer = result;
//     });
//   }

//   openBranch = () => {
//     // this.modalRef = this.modalService.show(BranchValidModalComponent);
//     // this.modalRef.content.data = { modalData: this.customer, correspondingFunction: 'openbranch' };
//     // this.modalRef.content.onClose.subscribe(result => {
//     //   this.customer = result;
//     // });
//     if (this.customer.solId) {
//       this.CommonServerService.FetchBranchBasedOnUser(this.customer.solId).subscribe(response => {
//         response = JSON.parse(this.CommonService.getDecryptedData(response));
//         console.log("response", response);
//         if (response['status'] === 'success') {
//           this.errorMsg['solId'] = '';
//           this.customer.branchName = response['data'].branchName;
//           this.customer.branchState = response['data'].branchStateCircle;
//           this.customer.branchSolId = response['data'].branchCode;
//         } else {
//           this.errorMsg['solId'] = 'Sol Id not found';
//           this.customer.branchName = '';
//         }
//       });
//     } else {
//       this.errorMsg['solId'] = 'Sol Id not found';
//       this.customer.branchName = '';
//     }
//   }



//   openImage = {};
//   URL;
//   viewUploadFiles1(fileName, mimetype, name) {
//     // this.viewFile = fileName.substring(fileName.indexOf('_') + 1);
//     fileName = fileName.substring(fileName.indexOf('_') + 1);

//     this.UploadFiles.uploadDownloadPost(fileName, mimetype).subscribe(
//       data => saveAs(data, name),
//       error => console.log(error)
//     );
//   }
//   viewUploadFiles(fileName, Encyption, mimetype) {
//     fileName = fileName.substring(fileName.indexOf('_') + 1);
//     this.UploadFiles.uploadViewPost(fileName, mimetype, response => {
//       var myWindow = window.open("", "ImageWindow", "width=1100, height=1000");
//       if (response.type === 'image') {
//         myWindow.document.write("<html><body oncontextmenu='return false;'><img src='data:" + mimetype + ";base64," + response.buffer + "' /></body></html>");
//       } else if (response.type === 'pdf') {
//         myWindow.document.write("<html><body><object width='1000' height='1000' data='data:" + mimetype + ";base64," + response.buffer + "' type='application/pdf'></object></body></html>");
//       }
//     }, error => {
//       console.log("error", error);
//     });
//   };
//   fileEvent(e: HTMLInputEvent) {
//     this.files = e.target.files[0];
//     this.uploadFile = () => {
//       if (!this.customer.customerId) {
//         this.mandatoryRefNo = true;
//         this.customer.docIdUp = '';
//         this.customer.documentDesc = '';
//       } else {
//         this.mandatoryRefNo = false;
//         this.UploadFiles.uploadFile(this.files, this.customer.docIdUp, this.customer.documentDesc, this.customer.customerId, 'Customer', this.customer.uploadedFiles, response => {
//           this.files = '';
//           this.customer.docIdUp = '';
//           this.customer.documentDesc = '';
//           this.fileUploadError = false;
//           this.fileUploadContent = false;
//           this.fileUploadSize = false;
//           this.fileUploadCrash = false;
//           this.fileDuplicate = false;
//         }, error => {
//           if (error === 'Maximum limit reached') {
//             this.files = '';
//             this.fileUploadSize = true;
//             this.fileUploadContent = false;
//             this.fileUploadCrash = false;
//             this.fileUploadError = false;
//             this.fileDuplicate = false;
//           } else if (error === 'Wrong Content') {
//             this.files = '';
//             this.fileUploadContent = true;
//             this.fileUploadSize = false;
//             this.fileUploadCrash = false;
//             this.fileUploadError = false;
//             this.fileDuplicate = false;
//           } else if (error === 'Server Error') {
//             this.files = '';
//             this.fileUploadCrash = true;
//             this.fileUploadContent = false;
//             this.fileUploadSize = false;
//             this.fileUploadError = false;
//             this.fileDuplicate = false;
//           } else if (error === 'Duplicate File') {
//             this.files = '';
//             this.fileUploadCrash = false;
//             this.fileUploadContent = false;
//             this.fileUploadSize = false;
//             this.fileUploadError = false;
//             this.fileDuplicate = true;
//           } else {
//             this.files = '';
//             this.fileUploadError = true;
//             this.fileUploadContent = false;
//             this.fileUploadSize = false;
//             this.fileUploadCrash = false;
//             this.fileDuplicate = false;
//           }
//         });
//       };
//     }
//   }
//   deleteFile = (fileName, i) => {
//     fileName = fileName.substring(fileName.indexOf('_') + 1);
//     this.UploadFiles.deleteFile2(fileName, this.customer.uploadedFiles, response => {
//       this.fileUploadError = false;
//       this.customer.uploadedFiles.splice(i, 1);
//     }, error => {
//       this.fileUploadError = true;
//     });
//   }
//   validationKey(key1) {
//     for (let i = 0; i < this.allValidationErrorsWithTabValue.length; i++) {
//       if (key1 === this.allValidationErrorsWithTabValue[i]['key']) {
//         this.keey1 = key1;
//         this.showTabPage(Number(this.allValidationErrorsWithTabValue[i]['value']));
//         this.hideTabs = true;
//         break;
//       }
//     }
//   }
//   nextTab(tab) {
//     this.showTabPage(tab + 1);
//   }
//   GoToNextTab() {
//     this.nextTab(this.tab);
//   }
//   // saveAndProceedNextTab(customerData) {
//   //   if (this.customer.customerId === '' || this.customer.customerId === null || this.customer.customerId === undefined ||  this.customerAlreadyExisted === true) {
//   //     this.errorMsg['customerId'] = 'CustomerId is required';
//   //     if (this.customer.customerId) {
//   //       this.errorMsg['customerId'] = '';
//   //     }
//   //   } else if (this.customer.customerId && this.customerAlreadyExisted === false) {

//   //     if (!String(this.customer.customerId).match(/^[a-zA-Z0-9\s+?/:.,()\'-]*$/)) {
//   //       this.errorMsg['customerId'] = 'Customer Id is Alpha Numeric';
//   //     } else {
//   //     this.errorMsg = {};
//   //     this.saveAndProceedAndExit(customerData);
//   //   }
//   // }
//   // }


//   saveAndProceedNextTab(customerData) {
//     this.customer.trxnStatus = 'Incomplete';
//     if (this.transactionMode === 'create') {
//       if (this.customer.customerId === undefined || this.customer.customerId === 'undefined' || this.customer.customerId === '') {
//         this.CommonServerService.getTransactionNumber(this.options, this.customer).subscribe(response => {
//           response = JSON.parse(this.CommonService.getDecryptedData(response));
//           this.customer.customerId = response['data'];
//           // console.log("this.customer herererer", this.customer);
//           // if (this.customer.customerId === '0') {
//           //   this.customer.customerId = '';
//           // }
//           this.CommonServerService.CustomerMPost(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//             response = JSON.parse(this.CommonService.getDecryptedData(response));
//             if (response['status'] === 'fail' || response['data'] === 'Record already exsists.') {
//               this.dbError = true;
//               this.showTabPage(0);
//               this.i = 0;
//               this.hideTabs = false;
//             } else {
//               this.dbError = false;
//               this.nextTab(this.tab);
//               this.transactionMode = 'update';
//             }
//           });
//         });
//       } else {
//         this.CommonServerService.CustomerMPost(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//           response = JSON.parse(this.CommonService.getDecryptedData(response));
//           if (response['status'] === 'fail' || response['data'] === 'Record already exsists.') {
//             this.dbError = true;
//             this.showTabPage(0);
//             this.i = 0;
//             this.hideTabs = false;
//           } else {
//             this.dbError = false;
//             this.nextTab(this.tab);
//           }
//         });
//       }
//     } else if (this.transactionMode === 'update') {
//       // this.sendDataOption = {
//       //   data: this.CommonService.getEncryptedData(this.customer)
//       // };
//       this.CommonServerService.CustomerMPut(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//         response = JSON.parse(this.CommonService.getDecryptedData(response));
//         if (response['status'] === 'fail' || response['data'] === 'Record already exsists.') {
//           this.dbError = true;
//           this.showTabPage(0);
//           this.i = 0;
//           this.hideTabs = false;
//         } else {
//           this.dbError = false;
//           this.nextTab(this.tab);
//         }
//       });
//     }
//   }


//   getFlowData(data) {
//     if (data.trxnStatus === 'review') {
//       this.flowData.trxnStatus = 'Reviewed';
//     } else if (data.trxnStatus === 'approve') {
//       this.flowData.trxnStatus = 'Approved';
//     } else if (data.trxnStatus === 'clarify') {
//       this.flowData.trxnStatus = 'Clarification';
//     } else {
//       this.flowData.trxnStatus = data.trxnStatus;
//     }
//     this.flowData.moduleName = this.options.moduleName;
//     this.flowData.transactionName = this.options.transactionName;
//     this.flowData.recordStatus = '';
//     this.flowData.userId = data.userIdCommon;
//     this.flowData.userName = data.userNameCommon;
//     this.flowData.refNo = data.customerId;
//   }

//   // saveAndExit(customerData) {
//   //   if (window.confirm('Are you sure want to exit?')) {
//   //     if (this.customer.customerId === '' || this.customer.customerId === null || this.customer.customerId === undefined || this.customerAlreadyExisted === true) {
//   //       this.errorMsg['customerId'] = 'CustomerId is required';
//   //       if (this.customer.customerId) {
//   //         this.errorMsg['customerId'] = '';
//   //       }
//   //     } else if (this.customer.customerId && this.customerAlreadyExisted === false) {
//   //       if (!String(this.customer.customerId).match(/^[a-zA-Z0-9\s+?/:.,()\'-]*$/)) {
//   //         this.errorMsg['customerId'] = 'Customer Id is Alpha Numeric';
//   //       } else {
//   //       this.errorMsg = {};

//   //       this.customer.userIdBranch = this.currentUserLogIn.user_id;
//   //       this.customer.userIdCommon = this.currentUserLogIn.user_id;
//   //       this.customer.userNameCommon = this.currentUserLogIn.user_name;
//   //       this.getFlowData(this.customer);
//   //       this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();

//   //       this.saveAndProceedAndExit(customerData);
//   //       this.router.navigate(['/dashboard'], {skipLocationChange: true});

//   //     }
//   //   }
//   //   }
//   // }

//   saveAndExit(customerData) {

//     if (this.transactionMode === 'create') {
//       if (this.customer.customerId === undefined || this.customer.customerId === 'undefined' || this.customer.customerId === '') {
//         this.CommonServerService.getTransactionNumber(this.options, this.customer).subscribe(response => {
//           response = JSON.parse(this.CommonService.getDecryptedData(response));
//           this.customer.customerId = response['data'];
//           this._modalService.setData(this.customer.customerId);
//           this.modalRef = this.modalService.show(BasicNewModalComponent);
//           this.modalRef.content.onClose.subscribe(result => {
//             if (result === 'Yes') {
//               this.customer.trxnStatus = 'Incomplete';
//               this.customer.userId = this.currentUserLogIn.user_id;
//               this.CommonServerService.CustomerMPost(this.CommonService.getEncryptedData(this.customer)).subscribe(payload => {
//                 payload = JSON.parse(this.CommonService.getDecryptedData(payload));
//                 console.log("payload in exit post", payload);

//                 this.customer.userIdBranch = this.currentUserLogIn.user_id;
//                 this.customer.userIdCommon = this.currentUserLogIn.user_id;
//                 this.customer.userNameCommon = this.currentUserLogIn.user_name;
//                 this.getFlowData(this.customer);
//                 this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();

//                 if (payload['status'] === 'fail') {
//                   this.dbError = true;
//                   this.showTabPage(0);
//                   this.hideTabs = false;
//                 } else {
//                   this.router.navigate(['/dashboard'], { skipLocationChange: true });
//                 }
//               });
//             }
//           });
//         });
//       } else {
//         this._modalService.setData(this.customer.customerId);
//         this.modalRef = this.modalService.show(BasicNewModalComponent);
//         this.modalRef.content.onClose.subscribe(result => {
//           if (result === 'Yes') {
//             this.customer.trxnStatus = 'Incomplete';
//             this.CommonServerService.CustomerMPost(this.CommonService.getEncryptedData(this.customer)).subscribe(payload => {
//               payload = JSON.parse(this.CommonService.getDecryptedData(payload));
//               this.customer.userIdBranch = this.currentUserLogIn.user_id;
//               this.customer.userIdCommon = this.currentUserLogIn.user_id;
//               this.customer.userNameCommon = this.currentUserLogIn.user_name;
//               this.getFlowData(this.customer);
//               this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();
//               if (payload['status'] === 'fail') {
//                 this.dbError = true;
//                 this.showTabPage(0);
//                 this.hideTabs = false;
//               } else {
//                 this.router.navigate(['/dashboard'], { skipLocationChange: true });
//               }
//             });
//           }
//         });
//       }
//     } else if (this.transactionMode === 'update') {
//       // this.sendDataOption = {
//       //   data: this.CommonService.getEncryptedData(this.customer)
//       //       };
//       this._modalService.setData(this.customer.customerId);
//       this.modalRef = this.modalService.show(BasicNewModalComponent);
//       this.modalRef.content.onClose.subscribe(result => {
//         if (result === 'Yes') {
//           this.customer.trxnStatus = 'Incomplete';
//           this.CommonServerService.CustomerMPut(this.CommonService.getEncryptedData(this.customer)).subscribe(payload => {
//             this.customer.userIdBranch = this.currentUserLogIn.user_id;
//             this.customer.userIdCommon = this.currentUserLogIn.user_id;
//             this.customer.userNameCommon = this.currentUserLogIn.user_name;
//             this.getFlowData(this.customer);
//             this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();
//             if (payload['status'] === 'fail') {
//               this.dbError = true;
//               this.showTabPage(0);
//               this.hideTabs = false;
//             } else {
//               this.router.navigate(['/dashboard'], { skipLocationChange: true });
//             }
//           });
//         }
//       });
//     }
//   }

//   // saveAndProceedAndExit(customerData) {
//   //   this.customer.userIdCommon = this.currentUserLogIn.user_id;
//   //   this.customer.trxnStatus = 'Incomplete';
//   //   if (this.transactionMode === 'create') {
//   //     this.CommonServerService.CustomerMPost(this.customer).subscribe(res => {
//   //       this.errorMsg = {};
//   //       this.nextTab(this.tab);
//   //       this.transactionMode = 'update';
//   //     });
//   //   } else if (this.transactionMode === 'update') {
//   //     this.sendDataOption = {
//   //       data: this.customer
//   //     };
//   //     this.CommonServerService.CustomerMPut(this.sendDataOption).subscribe(res => {
//   //       this.nextTab(this.tab);
//   //     });
//   //   }
//   // }
//   // finish(customerData) {
//   //   // customerData.trxnStatus = 'Confirm';
//   //   this.GlobalMraService.MraAccessFunction((data) => {
//   //     customerData.trxnStatus = data;
//   //   customerData.userId = this.currentUserLogIn.user_id;
//   //   this.customer.userIdCommon = this.currentUserLogIn.user_id;
//   //   this.allValidationErrors = [];
//   //   this.allValidationErrorsWithTabValue = [];
//   //   this.errorMsg = {};
//   //   if (this.transactionMode === 'create') {
//   //     if (this.customer.uploadedFiles.length === 0) {
//   //       this.repeatedData = {
//   //         AllData: 'Documentupload',
//   //         // dataduplicate: this.customer.email
//   //       }
//   //       this._modalService.setData(this.repeatedData);
//   //           this.modalRef = this.modalService.show(EmailCheckModalComponent);
//   //           this.modalRef.content.onClose.subscribe(result => {
//   //             if (result === 'Yes') {
//   //               this.CommonServerService.CustomerMPost(this.customer).subscribe(res => {



//   //                 if (res['status'] === 'validationError') {
//   //                   this.errorPresent = true;
//   //                   this.showTabPage(0);
//   //                   this.i = 0;
//   //                   this.hideTabs = false;
//   //                   this.validationErrorMethod(res['data']);
//   //                 } else if (res['status'] === 'success') {
//   //                   this.errorPresent = false;
//   //                   this.errorMsg = {};
//   //                   this.hideTabs = false;
//   //                   this.showTabPage(0);
//   //                   this.transactionMode = 'update';
//   //                   this.addedToMainDatabase = 'Data Saved successfully';

//   //                   this.customer.userIdBranch = this.currentUserLogIn.user_id;
//   //                   this.customer.userIdCommon = this.currentUserLogIn.user_id;
//   //                   this.customer.userNameCommon = this.currentUserLogIn.user_name;
//   //                   this.getFlowData(this.customer);
//   //                   this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();

//   //                 }
//   //               });
//   //             }
//   //           });
//   //         }else {
//   //           this.CommonServerService.CustomerMPost(this.customer).subscribe(res => {
//   //             if (res['status'] === 'validationError') {
//   //               this.errorPresent = true;
//   //               this.showTabPage(0);
//   //               this.i = 0;
//   //               this.hideTabs = false;
//   //               this.validationErrorMethod(res['data']);
//   //             } else if (res['status'] === 'success') {
//   //               this.errorPresent = false;
//   //               this.errorMsg = {};
//   //               this.hideTabs = false;
//   //               this.showTabPage(0);
//   //               this.transactionMode = 'update';
//   //               this.addedToMainDatabase = 'Data Saved successfully';
//   //               this.customer.userIdBranch = this.currentUserLogIn.user_id;
//   //               this.customer.userIdCommon = this.currentUserLogIn.user_id;
//   //               this.customer.userNameCommon = this.currentUserLogIn.user_name;
//   //               this.getFlowData(this.customer);
//   //               this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();
//   //             }
//   //           });
//   //         }
//   //   } else if (this.transactionMode === 'update') {
//   //     this.sendDataOption = {
//   //       data: this.customer
//   //     };
//   //     if (this.customer.uploadedFiles.length === 0) {
//   //       this.repeatedData = {
//   //         AllData: 'Documentupload',
//   //         // dataduplicate: this.customer.email
//   //       }
//   //       this._modalService.setData(this.repeatedData);
//   //           this.modalRef = this.modalService.show(EmailCheckModalComponent);
//   //           this.modalRef.content.onClose.subscribe(result => {
//   //             if (result === 'Yes') {
//   //               this.CommonServerService.CustomerMPut(this.sendDataOption).subscribe(res => {
//   //                 this.errorPresent = true;
//   //                 this.showTabPage(0);
//   //                 if (res['status'] === 'validationError') {
//   //                   this.errorPresent = true;
//   //                   this.showTabPage(0);
//   //                   this.hideTabs = false;
//   //                   this.i = 0;
//   //                   this.validationErrorMethod(res['data']);
//   //                 } else if (res['status'] === 'success') {
//   //                   this.errorPresent = false;
//   //                   this.errorMsg = {};
//   //                   this.hideTabs = false;
//   //                   this.showTabPage(0);
//   //                   this.addedToMainDatabase = 'Data Saved successfully';

//   //                   this.customer.userIdBranch = this.currentUserLogIn.user_id;
//   //                   this.customer.userIdCommon = this.currentUserLogIn.user_id;
//   //                   this.customer.userNameCommon = this.currentUserLogIn.user_name;
//   //                   this.getFlowData(this.customer);
//   //                   this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();

//   //                 }
//   //               });
//   //             }
//   //           });
//   //         }else {
//   //           this.CommonServerService.CustomerMPut(this.sendDataOption).subscribe(res => {
//   //             this.errorPresent = true;
//   //             this.showTabPage(0);
//   //             if (res['status'] === 'validationError') {
//   //               this.errorPresent = true;
//   //               this.showTabPage(0);
//   //               this.hideTabs = false;
//   //               this.i = 0;
//   //               this.validationErrorMethod(res['data']);
//   //             } else if (res['status'] === 'success') {
//   //               this.errorPresent = false;
//   //               this.errorMsg = {};
//   //               this.hideTabs = false;
//   //               this.showTabPage(0);
//   //               this.addedToMainDatabase = 'Data Saved successfully';
//   //               this.customer.userIdBranch = this.currentUserLogIn.user_id;
//   //               this.customer.userIdCommon = this.currentUserLogIn.user_id;
//   //               this.customer.userNameCommon = this.currentUserLogIn.user_name;
//   //               this.getFlowData(this.customer);
//   //               this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();

//   //             }
//   //           });
//   //         }
//   //   }
//   // });
//   // }

//   finish(customerData) {
//     console.log("customerID--------", this.customer.customerId);
//     this.customer.Advisestatus = 'Customer';

//     // this.GlobalMraService.MraAccessFunction((data) => {
//     this.customer.trxnStatus = 'Approved';
//     // customerData.trxnStatus = 'Confirm';
//     customerData.userId = this.currentUserLogIn.user_id;
//     this.customer.userIdCommon = this.currentUserLogIn.user_id;
//     this.customer.userNameCommon = this.currentUserLogIn.user_name;
//     this.allValidationErrors = [];
//     this.allValidationErrorsWithTabValue = [];
//     this.errorMsg = {};
//     if (this.transactionMode === 'create') {
//       if (this.customer.uploadedFiles.length === 0) {
//         this.repeatedData = {
//           AllData: 'Documentupload',
//           // dataduplicate: this.customer.telephone
//         }
//         this._modalService.setData(this.repeatedData);
//         this.modalRef = this.modalService.show(EmailCheckModalComponent);
//         this.modalRef.content.onClose.subscribe(result => {
//           if (result === 'Yes') {
//             if (this.customer.customerId === undefined || this.customer.customerId === 'undefined' || this.customer.customerId === '') {
//               this.CommonServerService.getTransactionNumber(this.options, this.customer).subscribe(response => {
//                 response = JSON.parse(this.CommonService.getDecryptedData(response));
//                 this.customer.customerId = response['data'];
//                 if (this.customer.customerId === '0') {
//                   this.customer.customerId = '';
//                 }
//                 this.CommonServerService.CustomerMPost(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//                   response = JSON.parse(this.CommonService.getDecryptedData(response));
//                   if (response['status'] === 'validationError') {
//                     this.errorPresent = true;
//                     this.showTabPage(0);
//                     this.i = 0;
//                     this.hideTabs = false;
//                     this.validationErrorMethod(response['data']);
//                   } else if (response['status'] === 'fail' || response['data'] === 'Record already exsists.') {
//                     this.dbError = true;
//                     this.showTabPage(0);
//                     this.i = 0;
//                     this.hideTabs = false;
//                   } else if (response['status'] === 'success') {
//                     this.errorPresent = false;
//                     this.errorMsg = {};
//                     this.hideTabs = false;
//                     this.showTabPage(0);
//                     this.addedToMainDatabase = "Saved Successfully";
//                     this.transactionMode = 'update';

//                     this.customer.userIdBranch = this.currentUserLogIn.user_id;
//                     this.customer.userIdCommon = this.currentUserLogIn.user_id;
//                     this.customer.userNameCommon = this.currentUserLogIn.user_name;
//                     this.getFlowData(this.customer);
//                     this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();


//                   }
//                 });
//               });
//             } else {
//               this.CommonServerService.CustomerMPost(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//                 response = JSON.parse(this.CommonService.getDecryptedData(response));
//                 if (response['status'] === 'validationError') {
//                   this.errorPresent = true;
//                   this.showTabPage(0);
//                   this.i = 0;
//                   this.hideTabs = false;
//                   this.validationErrorMethod(response['data']);
//                 } else if (response['status'] === 'fail' || response['data'] === 'Record already exsists.') {
//                   this.dbError = true;
//                   this.showTabPage(0);
//                   this.i = 0;
//                   this.hideTabs = false;
//                 } else if (response['status'] === 'success') {
//                   this.errorPresent = false;
//                   this.errorMsg = {};
//                   this.hideTabs = false;
//                   this.showTabPage(0);
//                   this.addedToMainDatabase = "Saved Successfully";
//                   this.transactionMode = 'update';

//                   this.customer.userIdBranch = this.currentUserLogIn.user_id;
//                   this.customer.userIdCommon = this.currentUserLogIn.user_id;
//                   this.customer.userNameCommon = this.currentUserLogIn.user_name;
//                   this.getFlowData(this.customer);
//                   this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();


//                 }
//               });
//             }
//           }
//         });
//       } else {
//         if (this.customer.customerId === undefined || this.customer.customerId === 'undefined' || this.customer.customerId === '') {
//           this.CommonServerService.getTransactionNumber(this.options, this.customer).subscribe(response => {
//             response = JSON.parse(this.CommonService.getDecryptedData(response));
//             this.customer.customerId = response['data'];
//             this.CommonServerService.CustomerMPost(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//               response = JSON.parse(this.CommonService.getDecryptedData(response));
//               if (response['status'] === 'validationError') {
//                 this.errorPresent = true;
//                 this.showTabPage(0);
//                 this.i = 0;
//                 this.hideTabs = false;
//                 this.validationErrorMethod(response['data']);
//               } else if (response['status'] === 'fail' || response['data'] === 'Record already exsists.') {
//                 this.dbError = true;
//                 this.showTabPage(0);
//                 this.i = 0;
//                 this.hideTabs = false;
//               } else if (response['status'] === 'success') {
//                 this.errorPresent = false;
//                 this.errorMsg = {};
//                 this.hideTabs = false;
//                 this.showTabPage(0);
//                 this.addedToMainDatabase = "Saved Successfully";
//                 this.transactionMode = 'update';

//                 this.customer.userIdBranch = this.currentUserLogIn.user_id;
//                 this.customer.userIdCommon = this.currentUserLogIn.user_id;
//                 this.customer.userNameCommon = this.currentUserLogIn.user_name;
//                 this.getFlowData(this.customer);
//                 this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();


//               }
//             });
//           });
//         } else {
//           this.CommonServerService.CustomerMPost(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//             response = JSON.parse(this.CommonService.getDecryptedData(response));
//             if (response['status'] === 'validationError') {
//               this.errorPresent = true;
//               this.showTabPage(0);
//               this.i = 0;
//               this.hideTabs = false;
//               this.validationErrorMethod(response['data']);
//             } else if (response['status'] === 'fail' || response['data'] === 'Record already exsists.') {
//               this.dbError = true;
//               this.showTabPage(0);
//               this.i = 0;
//               this.hideTabs = false;
//             } else if (response['status'] === 'success') {
//               this.errorPresent = false;
//               this.errorMsg = {};
//               this.hideTabs = false;
//               this.showTabPage(0);
//               this.addedToMainDatabase = "Saved Successfully";
//               this.transactionMode = 'update';

//               this.customer.userIdBranch = this.currentUserLogIn.user_id;
//               this.customer.userIdCommon = this.currentUserLogIn.user_id;
//               this.customer.userNameCommon = this.currentUserLogIn.user_name;
//               this.getFlowData(this.customer);
//               this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();


//             }
//           });
//         }
//       }
//     } else if (this.transactionMode === 'update') {
//       // this.sendDataOption = {
//       //   data: this.CommonService.getEncryptedData(this.customer)
//       // };
//       if (this.customer.uploadedFiles.length === 0) {
//         this.repeatedData = {
//           AllData: 'Documentupload',
//           // dataduplicate: this.customer.telephone
//         }

//         this._modalService.setData(this.repeatedData);
//         this.modalRef = this.modalService.show(EmailCheckModalComponent);
//         this.modalRef.content.onClose.subscribe(result => {
//           if (result === 'Yes') {
//             this.CommonServerService.CustomerMPut(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//               response = JSON.parse(this.CommonService.getDecryptedData(response));
//               if (response['status'] === 'validationError') {
//                 this.errorPresent = true;
//                 this.showTabPage(0);
//                 this.i = 0;
//                 this.hideTabs = false;
//                 this.validationErrorMethod(response['data']);
//               } else if (response['status'] === 'fail') {
//                 this.dbError = true;
//                 this.showTabPage(0);
//                 this.i = 0;
//                 this.hideTabs = false;
//               } else if (response['status'] === 'success') {
//                 this.errorPresent = false;
//                 this.errorMsg = {};
//                 this.hideTabs = false;
//                 this.showTabPage(0);
//                 this.mandatoryFields = false;
//                 this.addedToMainDatabase = "Saved Successfully";
//                 this.customer.userIdBranch = this.currentUserLogIn.user_id;
//                 this.customer.userIdCommon = this.currentUserLogIn.user_id;
//                 this.customer.userNameCommon = this.currentUserLogIn.user_name;
//                 this.getFlowData(this.customer);
//                 this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();
//                 this.AdviceService.adviceAccessFunction(this.customer, result => {
//                   // console.log("5555555555555555555");
//                   this.customer.Advisestatus = 'Customer';
//                   this.customer.customerId = this.customer.customerId;
//                   // console.log("this.customer.customerId---------",this.customer.customerId);
//                   this.customer.emailRefNumber = this.customer.customerId;
//                   // console.log("this.customer.customerId---------",this.customer.customerId);

//                   // this.emailError = response['EmailStatus'];
//                     // this.smsError = response['smsStaus'];
//                     this.CommonServerService.EmailSmsCommon(this.CommonService.getEncryptedData(this.customer)).subscribe(response1 => {
//                       response1 = JSON.parse(this.CommonService.getDecryptedData(response1));
//                       console.log("response1", response1);
//                       var emailErrorRes = response1;
//                       this.CommonServerService.SmsCommon(this.CommonService.getEncryptedData(this.customer)).subscribe(response2 => {
//                         response2 = JSON.parse(this.CommonService.getDecryptedData(response2));
//                         console.log("response1['EmailStatus']", emailErrorRes);
//                          this.emailError = emailErrorRes;
//                          this.smsError = response2['smsStaus'];
//                       });

//                     });

//                 });

//               }
//             });
//           }
//         });
//       } else {
//         this.CommonServerService.CustomerMPut(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//           response = JSON.parse(this.CommonService.getDecryptedData(response));
//           if (response['status'] === 'validationError') {
//             this.errorPresent = true;
//             this.showTabPage(0);
//             this.i = 0;
//             this.hideTabs = false;
//             this.validationErrorMethod(response['data']);
//           } else if (response['status'] === 'fail') {
//             this.dbError = true;
//             this.showTabPage(0);
//             this.i = 0;
//             this.hideTabs = false;
//           } else if (response['status'] === 'success') {
//             this.errorPresent = false;
//             this.errorMsg = {};
//             this.hideTabs = false;
//             this.showTabPage(0);
//             this.mandatoryFields = false;
//             this.addedToMainDatabase = "Saved Successfully";
//             this.customer.userIdBranch = this.currentUserLogIn.user_id;
//             this.customer.userIdCommon = this.currentUserLogIn.user_id;
//             this.customer.userNameCommon = this.currentUserLogIn.user_name;
//             this.getFlowData(this.customer);
//             this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();
//           }
//         });
//       }
//     }
//     // });
//   }




//   // display error method

//   validationErrorMethod1(resData) {
//     // tslint:disable-next-line:forin
//     for (let data in resData) {
//       let addValue = {};
//       let addTabValue = {};
//       let val = resData[data][this.i];
//       let splitted = val.split('-', 2);
//       addValue = { 'key': data, 'value': splitted[1] };
//       addTabValue = { 'key': data, 'value': splitted[0] };
//       this.allValidationErrorsWithTabValue.push(addTabValue);
//       this.allValidationErrors.push(addValue);
//       this.errorMsg1[data] = splitted[1];
//     }
//   }

//   validationErrorMethod(resData) {
//     // tslint:disable-next-line:forin
//     for (let data in resData) {
//       let addValue = {};
//       let addTabValue = {};
//       let val = resData[data][this.i];
//       let splitted = val.split('-', 2);
//       addValue = { 'key': data, 'value': splitted[1] };
//       addTabValue = { 'key': data, 'value': splitted[0] };
//       this.allValidationErrorsWithTabValue.push(addTabValue);
//       this.allValidationErrors.push(addValue);
//       this.errorMsg[data] = splitted[1];
//     }
//   }
//   review(customerData) {
//     if (!this.customer.remarksField) {
//       this.mandatoryClarifyRemarks = true;
//     } else {
//       customerData.trxnStatus = 'review';
//       customerData.reviewerId = this.currentUserLogIn.user_id;
//       this.customer.userIdCommon = this.currentUserLogIn.user_id;
//       this.customer.userNameCommon = this.currentUserLogIn.user_name;
//       // this.sendDataOption = {
//       //   data: this.CommonService.getEncryptedData(this.customer)
//       // };

//       this.customer.userIdBranch = this.currentUserLogIn.user_id;
//       this.customer.userIdCommon = this.currentUserLogIn.user_id;
//       this.customer.userNameCommon = this.currentUserLogIn.user_name;
//       this.getFlowData(this.customer);
//       this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();

//       this.CommonServerService.CustomerMPut(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//         response = JSON.parse(this.CommonService.getDecryptedData(response));
//         this.showTabPage(0);
//         this.hideTabs = false;
//         this.addedToMainDatabase = 'Reviewed successfully';
//       }, error => {
//         console.log('error is', error);
//       });
//     }
//   }

//   approve(customerData) {
//     this.customer.userIdCommon = this.currentUserLogIn.user_id;
//     this.customer.userNameCommon = this.currentUserLogIn.user_name;
//     if (!this.customer.remarksField) {
//       this.mandatoryClarifyRemarks = true;
//     } else {
//       customerData.trxnStatus = 'approve';
//       customerData.approverId = this.currentUserLogIn.user_id;
//       // this.sendDataOption = {
//       //   data: this.CommonService.getEncryptedData(this.customer)
//       // };


//       this.customer.userIdBranch = this.currentUserLogIn.user_id;
//       this.customer.userIdCommon = this.currentUserLogIn.user_id;
//       this.customer.userNameCommon = this.currentUserLogIn.user_name;
//       this.getFlowData(this.customer);
//       this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();
//       this.CommonServerService.CustomerMPut(this.CommonService.getEncryptedData(this.customer)).subscribe(response => {
//         response = JSON.parse(this.CommonService.getDecryptedData(response));
//         this.showTabPage(0);
//         this.hideTabs = false;
//         this.addedToMainDatabase = 'Approved successfully';
//       }, error => {
//         console.log('error is', error);
//       });
//     }
//   }

//   clarify = (data) => {
//     this.customer.userIdCommon = this.currentUserLogIn.user_id;
//     this.customer.userNameCommon = this.currentUserLogIn.user_name;
//     if (!this.customer.remarksField) {
//       this.mandatoryClarifyRemarks = true;
//     } else {
//       this.mandatoryClarifyRemarks = false;
//       if (this.transactionMode === 'review') {
//         this.customer.reviewerId = this.currentUserLogIn.user_id;
//       } else if (this.transactionMode === 'approve') {
//         this.customer.approverId = this.currentUserLogIn.user_id;
//       }

//       // this.sendDataOption = {
//       //   data: this.customer
//       // };
//       this.customer.trxnStatus = 'clarify';
//       this.customer.userIdBranch = this.currentUserLogIn.user_id;
//       this.customer.userIdCommon = this.currentUserLogIn.user_id;
//       this.customer.userNameCommon = this.currentUserLogIn.user_name;
//       this.getFlowData(this.customer);
//       this.CommonServerService.PostFlow(this.CommonService.getEncryptedData(this.flowData)).subscribe();
//       this.CommonServerService.customerClarify(this.CommonService.getEncryptedData(this.customer)).subscribe(payload => {
//         payload = JSON.parse(this.CommonService.getDecryptedData(payload));
//         if (payload['status'] === 'success') {
//           this.showTabPage(0);
//           this.hideTabs = false;
//           this.addedToMainDatabase = "Clarification details sent successfully";
//         } else if (payload['status'] === 'fail') {
//           this.showTabPage(0);
//           this.hideTabs = false;
//           // this.dbError = true;
//         }
//       });
//     }
//   }
//   focusout = () => {
//     if (this.customer.relationshipManagerId) {
//       // this.http.get('https://10.78.9.236:4400/AccDummy_routes/rmid/'+val.srcElement.value).toPromise().then(result => {
//       //   this.customer.relationshipManagerName = result['rmName'];
//       //   this.customer.emailForUpload = result['rmEmail'];
//       //   this.customer.emailForSupervisor = result['smEmail'];
//       //   });
//       this.customer.relationshipManagerName = 'Customer_RM_Name';
//       this.customer.emailForUpload = 's.bhavaraju@fintra.in';
//       this.customer.emailForSupervisor = 's.sankaran@fintra.in';
//     }
//   }
}
