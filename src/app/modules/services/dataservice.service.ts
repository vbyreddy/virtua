import { Injectable } from '@angular/core';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class DataserviceService {

  // private message = new BehaviorSubject<any>("");
  // Message = this.message.asObservable();
  constructor() { }
  
  receivedData : any = {};

  sendDatatoService(data){
    // console.log("ssssssssssaaaaaa", data);
    this.receivedData = data;
    // this.message.next(data);
    // console.log("bbbbbbbbbbb", this.receivedData);
  }
  receiveDatafromService(){
    // console.log("ssssss", this.receivedData);
    return this.receivedData;
  }
}
