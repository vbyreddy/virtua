import { Component, Directive, ElementRef, Renderer, HostListener  } from '@angular/core';
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'tab-common'
})
// tslint:disable-next-line:directive-class-suffix
export class TabCommon {
  message: string;
  constructor() {
  }
}

@Directive({
  selector: 'input, select, textarea'
})
export class MyInput {
  constructor(private _elRef: ElementRef, private _renderer: Renderer) {}
  focusIf(attrValue: string) {
    if (this._elRef.nativeElement.getAttribute('name') === attrValue) {
      this._renderer.invokeElementMethod(this._elRef.nativeElement, 'focus', []);
      return true;
    }
    return false;
  }
}
