import { Component, OnInit, ViewChild, EventEmitter, OnDestroy  } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { DataserviceService } from '../services/dataservice.service';
import { DatatransferService } from '../services/data-transfer.service';
import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
// import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-projectdetails',
  templateUrl: './projectdetails.component.html',
  styleUrls: ['./projectdetails.component.css']
})
export class ProjectdetailsComponent implements OnInit {
  projectDataDB : any = [];
  selectedItem : any = {};
  pageData : any = {};
  defaultPage : any = {};
  displayedColumns: string[] = ['projectId', 'cityFibreId', 'secondaryNodeId', 'locationId'];
  dataSource: any = [];
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private http: HttpClient,private dataTrans: DatatransferService) { }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  
  arrayData = [
    { sno:'1', name: 'ss1', sub:'s1'},
    { sno:'2', name: 'ss2', sub:'s2'},
    { sno:'3', name: 'ss3', sub:'s3'},
    { sno:'4', name: 'ss4', sub:'s4'},
    { sno:'5', name: 'ss5', sub:'s5'}
  ]

  ngOnInit() {
    // this.http.projectDataGetAll().subscribe(payload =>{
    //   if(payload[`status`] === 'success'){
    //     this.projectDataDB = payload[`data`];
    //   }      
    // })
    this.http.get(window.location.origin+'/projectDetails/entries').toPromise().then(payload => {
      console.log('payload', payload);
      // // payload = JSON.parse(this.CommonService.getDecryptedData(payload));
      // this.customerD = payload[`data`];
      this.projectDataDB = payload[`data`];
      this.dataSource = new MatTableDataSource(this.projectDataDB);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }).catch(error => {
      console.log('error', error);
    });
  }

  public selectedName:any;
  
  public highlightRow(emp) {
    console.log('empempempempemp', emp);
    this.selectedName = emp.projectId;
    this.selectedItem = emp;
  }

  customerView(typeValue, status) {
    this.defaultPage = false;

    if (typeValue === 'create') {
      this.pageData = { type: typeValue, disableAll: false, selectedItem: '', Status: status };
    } else if (typeValue === 'update') {
      this.pageData = { type: typeValue, disableAll: false, selectedItem: this.selectedItem, Status: status };
    } else if (typeValue === 'review' || typeValue === 'approve' || typeValue === 'enquiry') {
      this.pageData = { type: typeValue, disableAll: true, selectedItem: this.selectedItem, Status: status };
    }
    // this.servicesService.sendActionToPerform(this.pageData);
    this.dataTrans.sendDatatoService(this.pageData);
  }

}

export interface Element {
  flatNo: string;
  unit: number;
  routing: string;
  duct: string;
  cable: string;
}

const ELEMENT_DATA: Element[] = [
  { unit: 1, flatNo: '1', routing: 'DBX F1', duct: '', cable: "" },
  { unit: 2, flatNo: '2', routing: 'DBX F2', duct: '', cable: "" },
  { unit: 3, flatNo: '3', routing: 'DBX F3', duct: '', cable: "" },
  { unit: 4, flatNo: '4', routing: 'DBX F4', duct: '', cable: "" },
  { unit: 5, flatNo: '5', routing: 'DBX F5', duct: '', cable: "" },
  { unit: 6, flatNo: '6', routing: 'DBX F6', duct: '', cable: "" },
  { unit: 7, flatNo: '7', routing: 'DBX F7', duct: '', cable: "" },
  { unit: 8, flatNo: '8', routing: 'DBX F8', duct: '', cable: "" },
  { unit: 9, flatNo: '9', routing: 'DBX F9', duct: '', cable: "" },
  { unit: 10, flatNo: '10', routing: 'DBX F10', duct: '', cable: "" }
];

@Component({
  selector: 'app-Projectdetailsmain',
  templateUrl: './projectdetailsmain.component.html',
  styleUrls: ['./projectdetails.component.css']
})
export class ProjectdetailsmainComponent implements OnInit {

  constructor(private DataserviceService: DataserviceService, private http: HttpClient,private dataTrans: DatatransferService, private router: Router, private snackBar: MatSnackBar) { 
    this.serviceData = this.dataTrans.receiveDatafromService();
   }
  projectData : any = {};
  demo1TabIndex = 0;
  displayedColumns = ['unit', 'flatNo', 'routing', 'duct', 'cable'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  disableTabs : boolean = false;
  datee : any;
  dd : any;
  mm : any;
  yyyy : any;
  today : any;
  transactionMode : any;
  trxnMode : any;
  imageUrl: any;
  imageUrl2: any;
  imageUrl3: any;
  newArray = [];
  isLinear = true;
  subscription: Subscription;
  serviceData: any;
  showMsg: boolean = false;
  ngOnInit() {
    // console.log('this.dataTrans', this.dataTrans.result)
    // this.dataTrans.result.subscribe(message => {
    //   console.log('----===========================', message);
    //   this.serviceData = message
    // });
    
    console.log("this.serviceData--", this.serviceData);
    this.transactionMode = this.serviceData.type;
    this.trxnMode = this.serviceData.type;
    if(this.transactionMode === 'create'){
      // this.projectData = '';
    }else{
      this.http.get(window.location.origin+'/projectDetails/entries/'+this.serviceData.selectedItem.projectId).subscribe(payload =>{
      console.log(payload[`data`]);
      if(payload[`status`] === 'success'){
        this.projectData = payload[`data`][0];
      }
    
      // this.projectData = this.serviceData.selectedItem;
      if (this.projectData.image1) {
        const enc = new TextDecoder('utf-8');
        const arr = new Uint8Array(this.projectData.image1.data);
        this.imageUrl = enc.decode(arr);
        this.projectData.image1 = String(this.imageUrl);
      }
      if (this.projectData.image2) {
        const enc = new TextDecoder('utf-8');
        const arr = new Uint8Array(this.projectData.image2.data);
        this.imageUrl2 = enc.decode(arr);
        this.projectData.image2 = String(this.imageUrl2);
      }
      if (this.projectData.image3) {
        const enc = new TextDecoder('utf-8');
        const arr = new Uint8Array(this.projectData.image3.data);
        this.imageUrl3 = enc.decode(arr);
        this.projectData.image3 = String(this.imageUrl3);
      }
    })
    }      
      this.projectData.v1Survey = 'V1 Survey';
      this.projectData.v2Build = 'V2 Build';
      this.projectData.v3RedLight = 'V3 RedLight';
      this.datee = new Date();
      var today = new Date();
      this.dd = today.getDate();
      this.mm = today.getMonth() + 1; //January is 0!
      this.yyyy = today.getFullYear();

      if (this.dd < 10) {
        this.dd = '0' + this.dd;
      }
      if (this.mm < 10) {
        this.mm = '0' + this.mm;
      }
      this.today = this.dd + '-' + this.mm + '-' + this.yyyy;
  }
  SaveAndProceed(projectData) {
    // const tabCount = 4;
    // this.demo1TabIndex = (this.demo1TabIndex + 1) % tabCount;
    // this.projectData.trxnStatus = 'Incomplete';
    // if(this.transactionMode === 'create'){
    //   this.CommonserviceService.projectDataPost(this.projectData).subscribe(response =>{
    //     console.log("saveandproceedcreate--", response);
    //     this.transactionMode = 'update';
    //   });
    // }else{
    //   this.CommonserviceService.projectDataPut(this.projectData).subscribe(response =>{
    //     console.log("saveandproceedupdate--", response);
    //   });
    // }
  }
  finish(projectData){
    console.log('window.location.origin', window.location.origin);
    const tabCount = 4;
    this.demo1TabIndex = (this.demo1TabIndex + 1) % tabCount;
    this.disableTabs = true;
    this.projectData.trxnStatus = 'Approved';
    console.log("this.transactionMode", this.transactionMode);
    if(this.transactionMode === 'create'){
      this.http.post(window.location.origin+'/projectDetails/entries', this.projectData).subscribe(response =>{
        console.log("finishcreate--", response);
        this.transactionMode = 'update';
        this.router.navigate(['dashboard']);
        this.showMsg= true;
        this.snackBar.open(projectData.projectId+' has successfully created', '', {
          duration: 3000,
          verticalPosition:'top',
          panelClass: ['success-dialog']
        })
        // this.toastr.success('Hello world!', 'Toastr fun!');
      });
    }else{
      console.log('--------------------------------------------------------------');
      this.http.put(window.location.origin+'/projectDetails/entries', this.projectData).subscribe(response =>{
        console.log("finishupdate", response);
        this.router.navigate(['dashboard']);
        this.showMsg= true;
        this.snackBar.open(projectData.projectId+' has successfully updated', '', {
          duration: 3000,
          verticalPosition:'top',
          panelClass: ['success-dialog']
        })
        // this.toastr.success('Hello world!', 'Toastr fun!');
      });
    }
  }
  uploadFile1(event) {
    console.log("%%%%%%%%%%%%", event);
    const reader = new FileReader(); // HTML5 FileReader API
    const file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageUrl = [];
        this.imageUrl.push(reader.result);
        this.projectData.image1 = String(reader.result);
      };
    }
  }
  uploadFile2(event) {
    console.log("%%%%%%%%%%%%", event);
    const reader = new FileReader(); // HTML5 FileReader API
    const file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageUrl2 = [];
        this.imageUrl2.push(reader.result);
        this.projectData.image2 = String(reader.result);
      };
    }
  }
  uploadFile3(event) {
    console.log("%%%%%%%%%%%%", event);
    const reader = new FileReader(); // HTML5 FileReader API
    const file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageUrl3 = [];
        this.imageUrl3.push(reader.result);
        this.projectData.image3 = String(reader.result);
      };
    }
  }
  nextPage(){
    const tabCount = 4;
    this.demo1TabIndex = (this.demo1TabIndex + 1) % tabCount;
  }

  generatePdf(projectData) {
    console.log('hi I Entered!!!!!!', projectData);
    var projectId = JSON.stringify(this.projectData.projectId);
    var cityFibreId = JSON.stringify(this.projectData.cityFibreId);
    var secondaryNodeId = JSON.stringify(this.projectData.secondaryNodeId);
    var locationId = JSON.stringify(this.projectData.locationId);
    var UPRN = JSON.stringify(this.projectData.UPRN);
    var THP = JSON.stringify(this.projectData.THP);
    var extFeedDesign = JSON.stringify(this.projectData.extFeedDesign);
    var networkDesign = JSON.stringify(this.projectData.networkDesign);

    var v1Survey = JSON.stringify(this.projectData.v1Survey);
    var v1SurveyDate = this.projectData.v1SurveyDate;
    var v1SurveyName = JSON.stringify(this.projectData.v1SurveyName);
    var v1SurveyNumber = JSON.stringify(this.projectData.v1SurveyNumber);

    var v2Build = JSON.stringify(this.projectData.v2Build);
    var v2BuildDate = this.projectData.v2BuildDate;
    var v2BuildName = JSON.stringify(this.projectData.v2BuildName);
    var v2BuildNumber = JSON.stringify(this.projectData.v2BuildNumber);

    var v3RedLight = JSON.stringify(this.projectData.v3RedLight);
    var v3RedLightDate = this.projectData.v3RedLightDate;
    var v3RedLightName = JSON.stringify(this.projectData.v3RedLightName);
    var v3RedLightNumber = JSON.stringify(this.projectData.v3RedLightNumber);
    //0
    var unit0 = JSON.stringify(ELEMENT_DATA[0].unit);
    var flatNo0 = JSON.stringify(ELEMENT_DATA[0].flatNo);
    var routing0 = JSON.stringify(ELEMENT_DATA[0].routing);
    var duct0 = JSON.stringify(ELEMENT_DATA[0].duct);
    var cable0 = JSON.stringify(ELEMENT_DATA[0].cable);
    //1
    var unit1 = JSON.stringify(ELEMENT_DATA[1].unit);
    var flatNo1 = JSON.stringify(ELEMENT_DATA[1].flatNo);
    var routing1 = JSON.stringify(ELEMENT_DATA[1].routing);
    var duct1 = JSON.stringify(ELEMENT_DATA[1].duct);
    var cable1 = JSON.stringify(ELEMENT_DATA[1].cable);


    //2
    var unit2 = JSON.stringify(ELEMENT_DATA[2].unit);
    var flatNo2 = JSON.stringify(ELEMENT_DATA[2].flatNo);
    var routing2 = JSON.stringify(ELEMENT_DATA[2].routing);
    var duct2 = JSON.stringify(ELEMENT_DATA[2].duct);
    var cable2 = JSON.stringify(ELEMENT_DATA[2].cable);

    //3
    var unit3 = JSON.stringify(ELEMENT_DATA[3].unit);
    var flatNo3 = JSON.stringify(ELEMENT_DATA[3].flatNo);
    var routing3 = JSON.stringify(ELEMENT_DATA[3].routing);
    var duct3 = JSON.stringify(ELEMENT_DATA[3].duct);
    var cable3 = JSON.stringify(ELEMENT_DATA[3].cable);

    //4
    var unit4 = JSON.stringify(ELEMENT_DATA[4].unit);
    var flatNo4 = JSON.stringify(ELEMENT_DATA[4].flatNo);
    var routing4 = JSON.stringify(ELEMENT_DATA[4].routing);
    var duct4 = JSON.stringify(ELEMENT_DATA[4].duct);
    var cable4 = JSON.stringify(ELEMENT_DATA[4].cable);

    //5
    var unit5 = JSON.stringify(ELEMENT_DATA[5].unit);
    var flatNo5 = JSON.stringify(ELEMENT_DATA[5].flatNo);
    var routing5 = JSON.stringify(ELEMENT_DATA[5].routing);
    var duct5 = JSON.stringify(ELEMENT_DATA[5].duct);
    var cable5 = JSON.stringify(ELEMENT_DATA[5].cable);

    //6
    var unit6 = JSON.stringify(ELEMENT_DATA[6].unit);
    var flatNo6 = JSON.stringify(ELEMENT_DATA[6].flatNo);
    var routing6 = JSON.stringify(ELEMENT_DATA[6].routing);
    var duct6 = JSON.stringify(ELEMENT_DATA[6].duct);
    var cable6 = JSON.stringify(ELEMENT_DATA[6].cable);

    //7
    var unit7 = JSON.stringify(ELEMENT_DATA[7].unit);
    var flatNo7 = JSON.stringify(ELEMENT_DATA[7].flatNo);
    var routing7 = JSON.stringify(ELEMENT_DATA[7].routing);
    var duct7 = JSON.stringify(ELEMENT_DATA[7].duct);
    var cable7 = JSON.stringify(ELEMENT_DATA[7].cable);

    // 8 
    var unit8 = JSON.stringify(ELEMENT_DATA[0].unit);
    var flatNo8 = JSON.stringify(ELEMENT_DATA[8].flatNo);
    var routing8 = JSON.stringify(ELEMENT_DATA[8].routing);
    var duct8 = JSON.stringify(ELEMENT_DATA[8].duct);
    var cable8 = JSON.stringify(ELEMENT_DATA[8].cable);

    //9
    var unit9 = JSON.stringify(ELEMENT_DATA[9].unit);
    var flatNo9 = JSON.stringify(ELEMENT_DATA[9].flatNo);
    var routing9 = JSON.stringify(ELEMENT_DATA[9].routing);
    var duct9 = JSON.stringify(ELEMENT_DATA[9].duct);
    var cable9 = JSON.stringify(ELEMENT_DATA[9].cable);

    var imageOut1 = JSON.stringify(this.projectData.image1);
    var imageOut2 = JSON.stringify(this.projectData.image2);
    var imageOut3 = JSON.stringify(this.projectData.image3);

//function to convert the Image



    this.newArray.push(
      projectId.replace(/"/g, ""),           //0
      cityFibreId.replace(/"/g, ""),         //1
      secondaryNodeId.replace(/"/g, ""),     //2
      locationId.replace(/"/g, ""),          //3
      UPRN.replace(/"/g, ""),                //4
      THP.replace(/"/g, ""),                 //5
      extFeedDesign.replace(/"/g, ""),       //6
      networkDesign.replace(/"/g, ""),       //7
      //version 1
      v1Survey.replace(/"/g, ""),            //8
      v1SurveyDate,                          //9
      v1SurveyName.replace(/"/g, ""),        //10
      v1SurveyNumber.replace(/"/g, ""),      //11
      //version 2
      v2Build.replace(/"/g, ""),             //12
      v2BuildDate,                           //13
      v2BuildName.replace(/"/g, ""),         //14
      v2BuildNumber.replace(/"/g, ""),       //15
      //version 3
      v3RedLight.replace(/"/g, ""),          //16
      v3RedLightDate,                        //17
      v3RedLightName.replace(/"/g, ""),      //18
      v3RedLightNumber.replace(/"/g, ""),    //19
      //0
      unit0.replace(/"/g, ""),             //20
      flatNo0.replace(/"/g, ""),           //21
      routing0.replace(/"/g, ""),          //22
      duct0.replace(/"/g, ""),             //23
      cable0.replace(/"/g, ""),            //24
      //1
      unit1.replace(/"/g, ""),             //25
      flatNo1.replace(/"/g, ""),           //26
      routing1.replace(/"/g, ""),          //27
      duct1.replace(/"/g, ""),             //28
      cable1.replace(/"/g, ""),            //29

      //2
      unit2.replace(/"/g, ""),             //30
      flatNo2.replace(/"/g, ""),           //31
      routing2.replace(/"/g, ""),          //32
      duct2.replace(/"/g, ""),             //33
      cable2.replace(/"/g, ""),            //34
      //3
      unit3.replace(/"/g, ""),             //35
      flatNo3.replace(/"/g, ""),           //36
      routing3.replace(/"/g, ""),          //37
      duct3.replace(/"/g, ""),             //38
      cable3.replace(/"/g, ""),            //39
      //4
      unit4.replace(/"/g, ""),             //40
      flatNo4.replace(/"/g, ""),           //41
      routing4.replace(/"/g, ""),          //42
      duct4.replace(/"/g, ""),             //43
      cable4.replace(/"/g, ""),            //44
      //5
      unit5.replace(/"/g, ""),             //45
      flatNo5.replace(/"/g, ""),           //46
      routing5.replace(/"/g, ""),          //47
      duct5.replace(/"/g, ""),             //48
      cable5.replace(/"/g, ""),            //49
      //6
      unit6.replace(/"/g, ""),             //50
      flatNo6.replace(/"/g, ""),           //51
      routing6.replace(/"/g, ""),          //52
      duct6.replace(/"/g, ""),             //53
      cable6.replace(/"/g, ""),            //54
      //7
      unit7.replace(/"/g, ""),             //55
      flatNo7.replace(/"/g, ""),           //56
      routing7.replace(/"/g, ""),          //57
      duct7.replace(/"/g, ""),             //58
      cable7.replace(/"/g, ""),            //59
      //8
      unit8.replace(/"/g, ""),             //60
      flatNo8.replace(/"/g, ""),           //61
      routing8.replace(/"/g, ""),          //62
      duct8.replace(/"/g, ""),             //63
      cable8.replace(/"/g, ""),            //64
      //9
      unit9.replace(/"/g, ""),             //65
      flatNo9.replace(/"/g, ""),           //66
      routing9.replace(/"/g, ""),          //67
      duct9.replace(/"/g, ""),             //68
      cable9.replace(/"/g, ""),            //69

      imageOut1.replace(/"/g, ""),         //70
      imageOut2.replace(/"/g, ""),         //71
      imageOut3.replace(/"/g, ""),         //72
    ),
      console.log("this.newArray--->", this.newArray);

    var a = window.open('', '', 'height=1000, width=1000');
    a.document.write('<html style="padding-top: 89px;">');
    a.document.write('<body style="padding-left: 15px";>');
    //Table1
    a.document.write('<div>');
    a.document.write(' <table style="height:60px;width:325;float:left;border: 1px solid black;border-collapse: collapse;">');
    a.document.write('<tr>');
    a.document.write('<td  style="border-collapse: collapse;height:40px;width:300px;font-size:15px">ProjectId:' + this.newArray[0] + ' </td>');
    a.document.write('</tr>');

    a.document.write('<tr>');
    a.document.write('<td  style="border-collapse: collapse;height:40px;width:300px;font-size:15px">City Fibre Id:' + this.newArray[1] + ' </td>');
    a.document.write('</tr>');

    a.document.write('<tr>');
    a.document.write('<td  style="border-collapse: collapse;height:40px;width:300px;font-size:15px">Secondary Node Id:' + this.newArray[2] + ' </td>');
    a.document.write('</tr>');

    a.document.write('<tr>');
    a.document.write('<td  style="border-collapse: collapse;height:40px;width:300px;font-size:15px">Location Id:' + this.newArray[3] + ' </td>');
    a.document.write('</tr>');

    a.document.write('</table>');

    a.document.write(' <table style="height:60px;width:325;float:left;border: 1px solid black;border-collapse: collapse;">');
    a.document.write('<tr>');
    a.document.write('<td  style="border-collapse: collapse;height:40px;width:300px;font-size:15px">UPRN:' + this.newArray[4] + ' </td>');
    a.document.write('</tr>');

    a.document.write('<tr>');
    a.document.write('<td  style="border-collapse: collapse;height:40px;width:300px;font-size:15px">THP:' + this.newArray[5] + ' </td>');
    a.document.write('</tr>');

    a.document.write('<tr>');
    a.document.write('<td  style="border-collapse: collapse;height:40px;width:300px;font-size:15px">External Feed Design:' + this.newArray[6] + ' </td>');
    a.document.write('</tr>');

    a.document.write('<tr>');
    a.document.write('<td  style="border-collapse: collapse;height:40px;width:300px;font-size:15px">Network Design:' + this.newArray[7] + ' </td>');
    a.document.write('</tr>');

    a.document.write('</table>');
    a.document.write('</div>');

    //Table 2
    a.document.write('<div>');
    a.document.write('<table style="height:172px;width: 650px;border: 1px solid black; border-collapse: collapse;">');

    a.document.write('<tr>');
    a.document.write('<td style=" text-align:center;border: 1px solid black;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">Version Control  </td>');
    a.document.write('<td style="border: 1px solid black;text-align:center;height:10px;width:110px;font-size:15px">Date </td>');
    a.document.write('<td style="border: 1px solid black;text-align:center;height: 10px;width:110px;font-size:15px">Name</td>');
    a.document.write('<td style="border: 1px solid black;text-align:center;height: 10px;width:110px;font-size:15px"> Number</td>');
    a.document.write('</tr>');

    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[8]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[9]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[10]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[11]);
    a.document.write('</td>');
    a.document.write('</tr>');

    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[12]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[13]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[14]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[15]);
    a.document.write('</td>');
    a.document.write('</tr>');

    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[16]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[17]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[18]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[19]);
    a.document.write('</td>');
    a.document.write('</tr>');

    a.document.write('</table>');
    a.document.write('</div>');

    //table 3
    a.document.write('<div>');
    a.document.write('<table style="height:172px;width: 650px;border: 1px solid black; border-collapse: collapse;">');

    a.document.write('<tr>');
    a.document.write('<td style=" text-align:center;border: 1px solid black;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">Unit </td>');
    a.document.write('<td style="border: 1px solid black;text-align:center;height:10px;width:110px;font-size:15px">Flat No </td>');
    a.document.write('<td style="border: 1px solid black;text-align:center;height: 10px;width:110px;font-size:15px">Routing</td>');
    a.document.write('<td style="border: 1px solid black;text-align:center;height: 10px;width:110px;font-size:15px"> Duct</td>');
    a.document.write('<td style="border: 1px solid black;text-align:center;height: 10px;width:110px;font-size:15px"> Cable</td>');
    a.document.write('</tr>');
    //0
    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[20]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[21]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[22]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[23]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[24]);
    a.document.write('</td>');
    a.document.write('</tr>');
    //1
    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[25]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[26]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[27]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[28]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[29]);
    a.document.write('</td>');
    a.document.write('</tr>');

    //2 
    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[30]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[31]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[32]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[33]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[34]);
    a.document.write('</td>');
    a.document.write('</tr>');
    //3
    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[35]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[36]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[37]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[38]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[39]);
    a.document.write('</td>');
    a.document.write('</tr>');
    //4
    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[40]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[41]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[42]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[43]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[44]);
    a.document.write('</td>');
    a.document.write('</tr>');
    //5
    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[45]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[46]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[47]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[48]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[49]);
    a.document.write('</td>');
    a.document.write('</tr>');
    //6
    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[50]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[51]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[52]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[53]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[54]);
    a.document.write('</td>');
    a.document.write('</tr>');
    //7
    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[55]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[56]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[57]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[58]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[59]);
    a.document.write('</td>');
    a.document.write('</tr>');
    //8
    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[60]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[61]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[62]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[63]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[64]);
    a.document.write('</td>');
    a.document.write('</tr>');
    //9
    a.document.write(' <tr style=" text-align:center;border-collapse: collapse;width:170px;height: 10px;font-size:15px;font-style:bold;text-align:center;">');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[65]);
    a.document.write('</td>');
    a.document.write('<td style="height: 5px;width:100px;font-size:15px">' + this.newArray[66]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[67]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[68]);
    a.document.write('</td>');
    a.document.write(' <td style="height: 5px;width:100px;font-size:15px" >' + this.newArray[69]);
    a.document.write('</td>');
    a.document.write('</tr>');
    a.document.write('</table>');
    a.document.write('</div>');

    //Images
    a.document.write('<div>');
    a.document.write(' <table style="height:20px;width: 650px;border-collapse: collapse;">');
    a.document.write('<tr>');
    // a.document.write('<td  style="border: 1px solid black;border-collapse: collapse;height: 20px;width:350px;font-size:15px">');
    // a.document.write('</td>');

    // if (this.projectData.image2) {
    //   const enc = new TextDecoder('utf-8');
    //   const arr = new Uint8Array(this.projectData.image2.data);
    //   this.imageUrl2 = enc.decode(arr);
    //   this.projectData.image2 = String(this.imageUrl2);
    // }
    // if (this.projectData.image3) {
    //   const enc = new TextDecoder('utf-8');
    //   const arr = new Uint8Array(this.projectData.image3.data);
    //   this.imageUrl3 = enc.decode(arr);
    //   this.projectData.image3 = String(this.imageUrl3);
    // }

    a.document.write('<td  style=" border: 1px solid black;border-collapse: collapse;height: 20px;width:217px;font-size:15px">Image 1: <img class="image"  width="142" height="200" src='+this.newArray[70]+'>');//+ this.newArray[70]
    a.document.write('</td>');

    a.document.write('<td  style=" border: 1px solid black;border-collapse: collapse;height: 20px;width:217px;font-size:15px">Image 2:<img class="image"  width="142" height="200" src='+this.newArray[71]+'>');//+ this.newArray[71]
    a.document.write('</td>');

    a.document.write('<td  style=" border: 1px solid black;border-collapse: collapse;height: 20px;width:217px;font-size:15px">Image 3:<img class="image"  width="142" height="200" src='+this.newArray[72]+'>');// + this.newArray[72]
    a.document.write('</td>');

    a.document.write('</tr>');



    a.document.write('</table>');
    a.document.write('</div>');


    a.document.write('</body>');
    a.document.write('</html>');

    a.document.close();
    a.print();
  }
}
