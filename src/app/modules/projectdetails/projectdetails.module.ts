import { SharedModule } from '../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectdetailsComponent,ProjectdetailsmainComponent } from './projectdetails.component';
import { MatSidenavModule,
  MatTabsModule,
  MatCardModule,
  MatPaginatorModule,
  MatTableModule,
  MatDividerModule,
  MatSortModule,
  MatInputModule,
  MatFormFieldModule,
  MatStepperModule,
MatSnackBarModule } from '@angular/material';
  import { FormsModule } from '@angular/forms';

const routes : Routes = [
  { path : '', component : ProjectdetailsComponent },
  { path : 'viewpage', component : ProjectdetailsmainComponent }
]

@NgModule({
  declarations: [ ProjectdetailsComponent, ProjectdetailsmainComponent ],
  imports: [
    CommonModule,
    SharedModule,
    MatStepperModule,
    MatSidenavModule,
    MatTabsModule,
    MatCardModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class ProjectdetailsModule { }
