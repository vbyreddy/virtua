import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingpageComponent } from './drawingpage.component';

describe('DrawingpageComponent', () => {
  let component: DrawingpageComponent;
  let fixture: ComponentFixture<DrawingpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
