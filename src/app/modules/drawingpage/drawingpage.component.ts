import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import * as jspdf from 'jspdf';  
import html2canvas from "html2canvas"
import 'fabric';
declare const fabric: any;

class ImageSnippet {
  constructor(public src: string, public file: File) {}
}


@Component({
  selector: 'app-drawingpage',
  templateUrl: './drawingpage.component.html',
  styleUrls: ['./drawingpage.component.css']
})
export class DrawingpageComponent implements OnInit {
  canvas: any;
  img: any;
  selectedFile: ImageSnippet;
  imageLoader: any;
  url: any;
  test: true;
  isInit: boolean;
  line: any;
  isDown: any;
  origX: any;
  origY: any
  screenHeight: any;
  screenWidth: any;
  canvasDiv: any;
  public selectedValue: any; 

  constructor(private router: Router) { }

  @HostListener('document:keydown.delete', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.deleteActiveObject();
  }

  ngOnInit() {
    
    this.canvasDiv = document.getElementById('canvas');

    //Make the DIV element draggagle:
    this.dragElement(document.getElementById("mydiv"));

    this.canvas = new fabric.Canvas('canvas', { width: 1024, height: 768, preserveObjectStacking: true});
    this.canvas.add(
      new fabric.Rect({ top: 0, left: 0, width: this.canvasDiv.offsetWidth/3, height: 350, fill: 'transparent', stroke: 'black', strokeWidth: 2, selectable: false }),
      new fabric.Rect({ top: 0, left: this.canvasDiv.offsetWidth/3, width: (this.canvasDiv.offsetWidth/3), height: 350, fill: 'transparent', stroke: 'black', strokeWidth: 2, selectable: false}),
      new fabric.Rect({ top: 0, left: (this.canvasDiv.offsetWidth/3)*2, width: (this.canvasDiv.offsetWidth/3), height: 350, fill: 'transparent', stroke: 'black', strokeWidth: 2, selectable: false }),
    );
    this.canvas.on('object:selected', function(o){
      var activeObj = o.target;
      if(activeObj.get('type') == 'group') {
           activeObj.set({'borderColor':'#fbb802','cornerColor':'#fbb802'});
      
       }
      });
  }

  dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
      // if present, the header is where you move the DIV from:
      document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
      // otherwise, move the DIV from anywhere inside the DIV:
      elmnt.onmousedown = dragMouseDown;
    }
  
    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();
      // get the mouse cursor position at startup:
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      // call a function whenever the cursor moves:
      document.onmousemove = elementDrag;
    }
  
    function elementDrag(e) {
      // console.log();
      e = e || window.event;
      e.preventDefault();
      // calculate the new cursor position:
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      // set the element's new position:
      elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
      elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }
  
    function closeDragElement() {
      // stop moving when mouse button is released:
      document.onmouseup = null;
      document.onmousemove = null;
    }
  }
  
  
  deleteActiveObject() {
    var activeObject = this.canvas.getActiveObject();
    if (activeObject) {
      if (confirm("Sure you want to delete that?")) {
        this.canvas.remove(activeObject);
      }
    }
  }

  allowDrop(ev) {
    ev.preventDefault();
  }

  drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
  }

  drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
  }

  drawStars = (k,kk,index) => {
    // console.log(typeof k.result);
    if(index === 'upload'){
      fabric.Image.fromURL(k.result, function(img) {

      //   kk.setBackgroundImage(img, kk.renderAll.bind(kk), {
      //     scaleX: kk.width / img.width,
      //     scaleY: kk.height / img.height
      //  });
        img.set({ 'left': 50 });
        img.set({ 'top': 50 });
        img.scaleToWidth(100);
        img.scaleToHeight(100);
        img.transparentCorners = false
        kk.add(img);
        // console.log("KKKKKKKKKKKKKKKKKKKKKKK", kk);
      })
    }else{
      fabric.Image.fromURL(k.result, function(img) {
        img.set({ 'left': 150 });
        img.set({ 'top': 450 });
        img.scaleToWidth(30);
        img.scaleToHeight(30);
        img.transparentCorners = false

         kk.add(img);
        //  console.log("KKKKKKKKKKKKKKKKKKKKKKK", kk);
      })
    }
    
}

clickForText(){
  // console.log('--------------------------------------------');
  var textbox = new fabric.Textbox('Sample Text. Double click to edit', {
    left: 50,
    top: 50,
    width: 150,
    fontSize: 20,
    transparentCorners: false
  });
  this.canvas.add(textbox).setActiveObject(textbox);
}

  onSelectFile(event) { // called each time file input changes
    // console.log('event', event);
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event) => { // called once readAsDataURL is completed
          // console.log('event', event.target);
          // this.url = event.target.result;
          // // this.addImage(this.url);
          this.drawStars(event.target,this.canvas,'upload');
        }
      } else{
        this.toDataUrl(event.target.src, (result) =>{
          // console.log('------------------result------------', result);
          // this.canvas.add(result);
          this.drawStars({result:result},this.canvas,'tag');
        });
      }
  }

  toDataUrl = (url, callback) => {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
            callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }
  
}
