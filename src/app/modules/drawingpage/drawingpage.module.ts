import { SharedModule } from '../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrawingpageComponent } from './drawingpage.component';

const routes : Routes = [
  { path : '', component : DrawingpageComponent }
]

@NgModule({
  declarations: [ DrawingpageComponent ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class DrawingpageModule { }
