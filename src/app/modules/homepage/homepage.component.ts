import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private router: Router) { }
  
  centered = false;
  disabled = false;
  unbounded = false;
  radius: number;
  color: string;
  datee : any;
  dd : any;
  mm : any;
  yyyy : any;
  today : any;
  
  ngOnInit() {
    this.datee = new Date();
      var today = new Date();
      this.dd = today.getDate();
      this.mm = today.getMonth() + 1; //January is 0!
      this.yyyy = today.getFullYear();

      if (this.dd < 10) {
        this.dd = '0' + this.dd;
      }
      if (this.mm < 10) {
        this.mm = '0' + this.mm;
      }
      this.today = this.dd + '-' + this.mm + '-' + this.yyyy;
  }

  projectDetails(){
    this.router.navigate(['/dashboard/projectDetails'], {skipLocationChange : true});
  }
    canvasPage(){
    this.router.navigate(['/dashboard/drawingPage'], {skipLocationChange : true});
  }
  openDialog(){
    console.log("VIRTUA BOX");
  }
}
