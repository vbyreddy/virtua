import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router : Router) { }


  loginDetails = {};
  loginData : any = {};
  loginMessage : boolean = false;
  ngOnInit() {
  }
  loginPage(loginDetails){
    // console.log("loginDetails", loginDetails);
    if(loginDetails.userID === 'stuart' && loginDetails.password === 'kane'){
    this.router.navigate(['/dashboard'], {skipLocationChange : true});
    this.loginMessage = false;
    }else{
      this.loginData = "Incorrect Data";
      this.loginMessage = true;
    }
  }

}
