import { Component, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  isExpanded = true;
  showSubmenu: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;
  showMenu: any;
  showMenu1: any;
  showMenu2: any;
  showMenu3: any;
  openSideMenu() {

  };

  mouseenter() {
    if (!this.isExpanded) {
      this.isShowing = true;
    }
  }

  addExpandClass(element: any) {
    this.showMenu1 = '0';
    this.showMenu2 = '0';
    this.showMenu3 = '0';
    if (element === this.showMenu) {
        this.showMenu = '0';
    } else {
        this.showMenu = element;
    }
  }
  addExpandClass1(element: any) {
    this.showMenu2 = '0';
    this.showMenu3 = '0';
    if (element === this.showMenu1) {
        this.showMenu1 = '0';
    } else {
        this.showMenu1 = element;
    }
  }
  addExpandClass2(element: any) {
    this.showMenu1 = '0';
    this.showMenu3 = '0';
    if (element === this.showMenu2) {
        this.showMenu2 = '0';
    } else {
        this.showMenu2 = element;
    }
  }
  addExpandClass3(element: any) {
    this.showMenu1 = '0';
    this.showMenu2 = '0';
    if (element === this.showMenu3) {
        this.showMenu3 = '0';
    } else {
        this.showMenu3 = element;
    }
  }

  mouseleave() {
    if (!this.isExpanded) {
      this.isShowing = false;
    }
  }
  constructor() { }

  ngOnInit() {
  }


}





// @ViewChild('sidenav') sidenav: MatSidenav;
//   isExpanded = true;
//   showSubmenu: boolean = false;
//   isShowing = false;
//   showSubSubMenu: boolean = false;

//   mouseenter() {
//     if (!this.isExpanded) {
//       this.isShowing = true;
//     }
//   }

//   mouseleave() {
//     if (!this.isExpanded) {
//       this.isShowing = false;
//     }
//   }
