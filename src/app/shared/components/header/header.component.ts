import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  datee : any;
  dd : any;
  mm : any;
  yyyy : any;
  today : any;
 
  constructor(private router: Router) { }

  ngOnInit() {
    this.datee = new Date();
      var today = new Date();
      this.dd = today.getDate();
      this.mm = today.getMonth() + 1; //January is 0!
      this.yyyy = today.getFullYear();

      if (this.dd < 10) {
        this.dd = '0' + this.dd;
      }
      if (this.mm < 10) {
        this.mm = '0' + this.mm;
      }
      this.today = this.dd + '-' + this.mm + '-' + this.yyyy;
  }

  logout = () =>{
    // console.log('-------------------------------------------------');
    this.router.navigate([''], {skipLocationChange : true});
  }

  toggleSideBar() {
    // console.log('this.toggleSideBarForMe', this.toggleSideBarForMe);
    // console.log('-------------------------------------');
    this.toggleSideBarForMe.emit();
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }

}
