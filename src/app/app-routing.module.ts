import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layouts/default/default.component';
import { PostsComponent } from './modules/posts/posts.component';
import { LoginComponent } from './modules/login/login.component';

const routes: Routes = [{
  path: '',
  component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DefaultComponent,
    children: [{
      path: '', loadChildren: () => import(`./modules/homepage/homepage.module`).then(m=>m.HomepageModule)
    }, {
      path: 'projectDetails', loadChildren: () => import(`./modules/projectdetails/projectdetails.module`).then(m=>m.ProjectdetailsModule)
    }, {
      path: 'drawingPage', loadChildren: () => import(`./modules/drawingpage/drawingpage.module`).then(m=>m.DrawingpageModule)
    },{
      path: 'filterCustomer', loadChildren: () => import(`./modules/Masters/Customer/customer.module`).then(m => m.CustomerModule)
    }]
    }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
