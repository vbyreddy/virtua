import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DefaultComponent } from './default.component';
import { RouterModule } from '@angular/router';
import { PostsComponent } from 'src/app/modules/posts/posts.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatSidenavModule,
  MatTabsModule, MatCardModule,
  MatPaginatorModule, MatTableModule,
  MatDividerModule, MatFormFieldModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DashboardComponent } from 'src/app/modules/dashboard/dashboard.component';
import { TabCommon, MyInput } from 'src/app/modules/Services/tab-common';

@NgModule({
  declarations: [
    DefaultComponent,
    DashboardComponent,
    PostsComponent,
    MyInput,
    TabCommon
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    FlexLayoutModule,
    MatSidenavModule,
    MatTabsModule,
    MatCardModule,
    MatPaginatorModule,
    MatTableModule,
    MatDividerModule,
    MatFormFieldModule,
    FormsModule
  ],
  exports: [
    MyInput,
    TabCommon
  ]
})
export class DefaultModule { }
