var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var http = require('http');
var cors = require('cors');
var app = express();
require('dotenv').config()
app.use(cors());
// app.use(bodyParser.json());
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({ extended: true}));
app.use(express.static(path.join(__dirname, 'dist')));
app.get('*'), (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/angular8/index.html'));
}

var projectDetails = require('./routes/projectDetails/projectDetails_routes');

app.use('/projectDetails', projectDetails);

const port = process.env.PORT || '4200';
app.set('port', port);

const server = http.createServer(app);

var url = process.env.URL;
// console.log("URL", url);
server.listen(port, () => console.log(`Server Running on localhost:${port}`));