var db1 = require('../configuration/settings');
var sworm = require('sworm');
var logger = require('../services/logger');

// var configuration = { driver: 'mysql', config: { user: 'root', password: 'fintra123', database: 'virtua_db', host: 'virtua.fintra.in', port:3306 } };

var configuration = { driver: 'mysql', config: { user: 'root', password: 'tiger', database: 'virtua_db', host: '192.168.1.49', port:3306 } };// var conversionFile = require('./convert');
var dbName = configuration.driver;
// var dbConfig = db.config;
var orm = abstract_sworn = {};
orm.query = function (options) {
    var db = sworm.db(configuration);

    var person = db.model(options.model);
    // console.log('--------------------------------------------');
    db.connect(function () {
        // connected       
        //  console.log("Connected ------------- query");
        var response1 = [];
        person.query(options.query).then((data) => {
            var processedData;
            if (dbName === 'oracle') {
                processedData = conversionFile.convert(data, response1, options.tableName);
            } else {
                processedData = data;
            }
            return options.resolve(processedData);
        }, error => {
            logger.logger.error(error);
            // console.log('error is', error);
            error = 'dbError';
            return options.resolve(error);
        });
    }).then(function () {
        //  console.log("Disconnected ------------- query");
        // disconnected     

    }).catch((err) => {
        logger.logger.error(JSON.stringify(err));
    });

}
orm.queryById = function (options, id) {
    var db = sworm.db(configuration);

    var person = db.model(options.model);
    db.connect(function () {
        // console.log("ididididid", id);
        // console.log('options.model', options.model);
        // connected       
        // console.log("Connected ---------------- queryById");
        var response1 = [];
        try {
            person.query(options.query, id).then((data) => {
                var processedData;
                if (dbName === 'oracle') {
                    processedData = conversionFile.convert(data, response1, options.tableName);
                } else {
                    processedData = data;
                }
                return options.resolve(processedData);
            });
        } catch (error) {
            logger.logger.error(error);
            console.log('error', error);
            error = 'dbError';
            return options.resolve(error);
        }
    }).then(function () {
        // console.log("Disconnected ------------- queryById");
        // disconnected       

    }).catch((err) => {
        logger.logger.error(JSON.stringify(err));
    });
}
orm.create = function (options, dataObj) {
    var db = sworm.db(configuration);
    // console.log('options.model create', options.model, dataObj);
    var person = db.model(options.model);
    db.connect(function () {
        // connected       
        // console.log("Connected ------------create");
        var response1 = [];
        try {
            // person(dataObj).save();
            // console.log('TRYTRYTRY');

            var bob = person(dataObj);
            // console.log('bobbobbobbob', bob);
            return bob.save();
        } catch (error) {
            logger.logger.error(error);
            // console.log('catchcatchcatch');
            dataObj = 'dbError';
            return;
        }

    }).then(function () {
        // console.log("Disconnected ---------------- create");
        // disconnected       
        options.resolve(dataObj)
    }).catch((err) => {
        logger.logger.error(JSON.stringify(err));
    });

}
orm.update = function (options, dataObj) {
    var db = sworm.db(configuration);
    // console.log('options.model update', options.model, dataObj);
    var person = db.model(options.model);
    db.connect(function () {
        // connected       
        // console.log("Connected ------------ update", dbName);
        try {
            if (dbName === 'mysql') {
                return person(dataObj).update();
            } else {
                return dataObj.update();
            }
        } catch (error) {
            logger.logger.error(error);
            console.log('If error in update', error);
            dataObj = error;
            return;
        }
    }).then(function () {
        // console.log("Disconnected ================ update");
        // disconnected       
        options.resolve(dataObj)
    }).catch((err) => {
        logger.logger.error(JSON.stringify(err));
    });

}
orm.statementById = function (options, id) {
    var db = sworm.db(configuration);
    var person = db.model(options.model);

    db.connect(function () {
        // connected       
        // console.log("Connected ----------------- statementById");
        try {
            db.statement(options.query, id).then(data => {
                return options.resolve(data);
            });
        } catch (error) {
            logger.logger.error(error);
            console.log('If error in statementBy id', error);
            error = 'dbError';
            return options.resolve(error);
        }
    }).then(function () {
        // console.log("Disconnected -------------- statementById");
        // disconnected       

    }).catch((err) => {
        logger.logger.error(JSON.stringify(err));
    });

}
orm.updateByQuery = function (options, id) {
    var db = sworm.db(configuration);
    db.connect(function () {
        // connected       
        // console.log("Connected ----------------- statementById");
        try {
            db.statement(options.query, id).then(data => {
                return options.resolve(data);
            });
        } catch (error) {
            logger.logger.error(error);
            // console.log('If error in statementBy id', error);
            error = 'dbError';
            return options.resolve(error);
        }
    }).then(function () {
        // console.log("Disconnected -------------- statementById");
        // disconnected
    }).catch((err) => {
        logger.logger.error(JSON.stringify(err));
    });
}
module.exports = abstract_sworn;