var _ = require('lodash');
var orm = require('../../ormModule');

module.exports = {
    create : (modelObj,data,next)=>{
        var params = _.pick(data, 'projectId', 'cityFibreId', 'secondaryNodeId', 'locationId', 'UPRN', 'THP', 'ExtFeedDesign', 'networkDesign', 'trxnStatus', 'v1Survey','v1SurveyDate','v1SurveyName','v1SurveyNumber','v2Build','v2BuildDate','v2BuildName','v2BuildNumber','v3RedLight','v3RedLightDate','v3RedLightName','v3RedLightNumber','fileTitle','uploadfile','image1','image2','image3');
        var options = {
            model: modelObj,
            resolve: function(projectDetailsData) {
                var response = {};
                if (projectDetailsData !== 'dbError') {
                    response.status = 'success';
                    response.data = projectDetailsData;
                    next(response);
                } else if (projectDetailsData === 'dbError') {
                    response.status = 'dbError';
                    response.data = projectDetailsData;
                    next(response);
                } else {
                    response.status = 'fail';
                    response.data = '';
                    next(response);
                }
            }
        }
        orm.create(options, params);
    },
    getAll: function(modelObj, data, next) {
        var options = {
            model: modelObj,
            tableName: 'projectDetails',
            query: "SELECT projectId,cityFibreId,secondaryNodeId,locationId,trxnStatus from projectDetails",
            resolve: function(projectData) {
                var response = {};
                if (projectData.length !== 0 && projectData !== 'dbError') {
                    response.status = 'success';
                    response.data = projectData;
                    next(response);
                } else if (projectData === 'dbError') {
                    response.status = 'dbError';
                    response.data = projectData;
                    next(response);
                } else {
                    response.status = 'fail';
                    response.data = '';
                    next(response);
                }
            }
        }
        orm.query(options);
    },
    getById: function(modelObj, data, next) {
        console.log('data', data)
        var options = {
            model: modelObj,
            tableName: 'projectDetails',
            query: "SELECT * from projectDetails WHERE projectId=@id",
            resolve: function(projectData) {
                var response = {};
                console.log('projectData', projectData);
                if (projectData.length !== 0 && projectData !== 'dbError') {
                    response.status = 'success';
                    response.data = projectData;
                    next(response);
                } else if (projectData === 'dbError') {
                    response.status = 'dbError';
                    response.data = projectData;
                    next(response);
                } else {
                    response.status = 'fail';
                    response.data = '';
                    next(response);
                }
            }
        }
        orm.queryById(options, {
            id: data.id
        });
    },
    update: function(modelObj, data, next) {
    console.log("UPDATS IS CALLED", data);
        // var options = {
        //     model: modelObj,
        //     tableName: 'projectDetails',
        //     query: "SELECT * from projectDetails WHERE projectId=@id",
        //     resolve: function(projectData) {
        //         var response = {};
        //         if (projectData.length !== 0 && projectData !== 'dbError') {
        //             projectData[0].cityFibreId = data.cityFibreId;
        //             projectData[0].secondaryNodeId = data.secondaryNodeId;
        //             projectData[0].locationId = data.locationId;
        //             projectData[0].UPRN = data.UPRN;
        //             projectData[0].THP = data.THP;
        //             projectData[0].extFeedDesign = data.extFeedDesign;
        //             projectData[0].networkDesign = data.networkDesign;
        //             projectData[0].v1Survey = data.v1Survey;
        //             projectData[0].v1SurveyDate = data.v1SurveyDate;
        //             projectData[0].v1SurveyName = data.v1SurveyName;
        //             projectData[0].v1SurveyNumber = data.v1SurveyNumber;
        //             projectData[0].v2Build = data.v2Build;
        //             projectData[0].v2BuildDate = data.v2BuildDate;
        //             projectData[0].v2BuildName = data.v2BuildName;
        //             projectData[0].v2BuildNumber = data.v2BuildNumber;
        //             projectData[0].v3RedLight = data.v3RedLight;
        //             projectData[0].v3RedLightDate = data.v3RedLightDate;
        //             projectData[0].v3RedLightName = data.v3RedLightName;
        //             projectData[0].v3RedLightNumber = data.v3RedLightNumber;
        //             projectData[0].fileTitle = data.fileTitle;
        //             projectData[0].uploadfile = data.uploadfile;
        //             projectData[0].trxnStatus = data.trxnStatus;
        //             projectData[0].image1 = data.image1;
        //             projectData[0].image2 = data.image2;
        //             projectData[0].image3 = data.image3;
                    var options1 = {
                        model: modelObj,
                        tableName: 'projectDetails',
                        resolve: function(data) {
                            console.log('=------------------------------=--=-=-=-==--=-=-=-=-=-=-=-');
                            if (data !== 'dbError') {
                                response.status = 'success';
                                response.data = data;
                                next(response);
                            } else if (data === 'dbError') {
                                response.status = 'dbError';
                                response.data = data;
                                next(response);
                            } else {
                                response.status = 'fail';
                                response.data = data;
                                next(response);
                            }
                        }
                    }
                    orm.update(options1, data);
        //         } else if (projectData === 'dbError') {
        //             response.status = 'dbError';
        //             response.data = projectData;
        //             next(response);
        //         } else {
        //             response.status = 'fail';
        //             response.data = 'No Record Found';
        //             next(response);
        //         }
        //     }
        // }
        // orm.queryById(options, {
        //     id: data.projectId
        // });
    },
}