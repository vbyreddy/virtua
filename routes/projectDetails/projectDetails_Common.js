var db = require('../../configuration/settings');
var orm = require('../../orm/controllers');
var projectDetails = db.settings.model({table : 'projectDetails', id : ['projectId']});

module.exports = {
    getAll : function(req,res,next){
        response = {};
        var data = req.body;
        var modelObj = projectDetails;
        var cb = (response)=>{
            res.send(response)
        }
        orm.projectDetails.getAll(modelObj, data, cb);
    },
    getById : function(req,res,next){
        response = {};
        var data = req.params;
        var modelObj = projectDetails;
        var cb = (response)=>{
            res.send(response)
        }
        orm.projectDetails.getById(modelObj, data, cb);
    },
    post : function(req,res,next){
        response = {};
        var data = req.body;
        var modelObj = projectDetails;
        var cb = (response)=>{
            res.send(response)
        }
        orm.projectDetails.create(modelObj, data, cb);
    },
    put : function(req,res,next){
        response = {};
        var data = req.body;
        var modelObj = projectDetails;
        var cb = (response)=>{
            res.send(response)
        }
        orm.projectDetails.update(modelObj, data, cb);
    }
}