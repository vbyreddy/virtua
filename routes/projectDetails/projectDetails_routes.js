var express = require('express');
var router = express.Router();
var commonFile = require('./projectDetails_Common');

router.get('/entries', (req,res,next)=>{
    var cb = function(response) {
        res.send(response);
    }
    commonFile.getAll(req,res,cb);
})

router.get('/entries/:id', (req,res,next)=>{

    // console.log(req.params, req.query);
    var cb = function(response) {
        res.send(response);
    }
    commonFile.getById(req,res,cb);
})

router.post('/entries', (req,res,next)=>{
    var response = {};
    var cb = function(response) {
        res.send(response);
    }
    commonFile.post(req,res,cb);
})

router.put('/entries', (req,res,next)=>{
    var response = {};
    var cb = function(response) {
        res.send(response);
    }
    commonFile.put(req,res,cb);
})

module.exports = router;